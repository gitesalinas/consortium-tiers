﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.Contract
{
    public interface IRoomFacilityRepository
    {
        IEnumerable<RoomFacility> GetAll(short status = (short)EnumStatus.None);
        RoomFacility Get(int id, short status = (short)EnumStatus.None);

        RoomFacility GetAny(int id);

        RoomFacility GetAnyByRoomIdAndFacilityId(int roomId, int facilityId);

        IEnumerable<RoomFacility> GetAllByRoomId(int roomId, short status = (short)EnumStatus.None);

        void Save(RoomFacility roomFacility);

        void Delete(RoomFacility roomFacility);
    }
}
