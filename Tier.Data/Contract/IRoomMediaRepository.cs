﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.Contract
{
    public interface IRoomMediaRepository
    {
        IEnumerable<RoomMedia> GetAll(short status = (short)EnumStatus.None);
        RoomMedia Get(int id, short status = (short)EnumStatus.None);

        IEnumerable<RoomMedia> GetAllByRoomId(int roomId, short status = (short)EnumStatus.None);

        void Save(RoomMedia roomMedia);

        void Delete(RoomMedia roomMedia);

        void HandleMain(RoomMedia roomMedia);

        RoomMedia GetMainRoomMedia(int roomId);

        void AddRoomMedia(RoomMedia roomMedia);
    }
}
