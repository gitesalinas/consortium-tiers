﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.Contract
{
    public interface IProductRepository
    {
        IEnumerable<BaseProduct> GetAllFull(short status = (short)EnumStatus.None);
        IEnumerable<BaseProduct> GetAll(short status = (short)EnumStatus.None);

        BaseProduct GetFull(int id, short status = (short)EnumStatus.None);
        BaseProduct Get(int id, short status = (short)EnumStatus.None);

        void Save(BaseProduct product);

        void Delete(BaseProduct product);
    }
}
