﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.Contract
{
    public interface IProductFurnitureRepository
    {
        IEnumerable<ProductFurniture> GetAllFull(short status = (short)EnumStatus.None);
        IEnumerable<ProductFurniture> GetAll(short status = (short)EnumStatus.None);

        ProductFurniture GetFull(int id, short status = (short)EnumStatus.None);
        ProductFurniture Get(int id, short status = (short)EnumStatus.None);

        void Save(ProductFurniture product);

        void Delete(ProductFurniture product);

        IEnumerable<ProductFurniture> GetAllForRelationship(int productId, short status = (short)EnumStatus.None);
    }
}
