﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.Contract
{
    public interface IUserRepository
    {
        User GetFull(int id, short status = (short)EnumStatus.None);
        IEnumerable<User> GetAllFull(short status = (short)EnumStatus.None);

        User Get(int id, short status = (short)EnumStatus.None);
        IEnumerable<User> GetAll(short status = (short)EnumStatus.None);

        User GetProfile(int id);
        User GetWithPerson(int id);

        User Login(string username, string lastname);

        void Register(User user);
        void SaveProfile(User user);
        void SaveProfilePicture(User user, Resource resource);
        void Save(User user);

        void UpdateStatus(int id, short status = (short)EnumStatus.None);

        void Delete(int id);
    }
}
