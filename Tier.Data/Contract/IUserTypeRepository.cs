﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.Contract
{
    public interface IUserTypeRepository
    {
        UserType Get(int id);
        IEnumerable<UserType> GetAll(short status = (short)EnumStatus.None);

        void Save(UserType userType);

        void Delete(UserType userType);
    }
}
