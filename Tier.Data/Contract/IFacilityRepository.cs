﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.Contract
{
    public interface IFacilityRepository
    {
        IEnumerable<Facility> GetAll(short status = (short)EnumStatus.None);
        Facility Get(int id, short status = (short)EnumStatus.None);

        void Save(Facility facility);

        void Delete(Facility facility);
    }
}
