﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.Contract
{
    public interface IRoomRepository
    {
        IEnumerable<Room> GetAllFull(short status = (short)EnumStatus.None);
        IEnumerable<Room> GetAll(short status = (short)EnumStatus.None);

        Room GetFull(int id, short status = (short)EnumStatus.None);
        Room Get(int id, short status = (short)EnumStatus.None);

        void Save(Room room);
        void Delete(Room room);
        //void AddRoomMedia(RoomMedia roomMedia);
    }
}
