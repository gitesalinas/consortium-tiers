﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.Contract
{
    public interface IProductTypeRepository
    {
        ProductType Get(int id);
        IEnumerable<ProductType> GetAll(short status = (short)EnumStatus.None);

        void Save(ProductType productType);

        void Delete(ProductType productType);
    }
}
