﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.Contract
{
    public interface IProductFurnitureMediaRepository
    {
        IEnumerable<ProductFurnitureMedia> GetAll(short status = (short)EnumStatus.None);
        ProductFurnitureMedia Get(int id, short status = (short)EnumStatus.None);

        IEnumerable<ProductFurnitureMedia> GetAllByProductId(int productId, short status = (short)EnumStatus.None);

        void Save(ProductFurnitureMedia productMedia);

        void Delete(ProductFurnitureMedia productMedia);

        void HandleMain(ProductFurnitureMedia productMedia);

        ProductFurnitureMedia GetMainProductMedia(int productId);

        void AddProductMedia(ProductFurnitureMedia productMedia);
    }
}
