﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.Contract
{
    public interface IProductLoggingMediaRepository
    {
        IEnumerable<ProductLoggingMedia> GetAll(short status = (short)EnumStatus.None);
        ProductLoggingMedia Get(int id, short status = (short)EnumStatus.None);

        IEnumerable<ProductLoggingMedia> GetAllByProductId(int productId, short status = (short)EnumStatus.None);

        void Save(ProductLoggingMedia productMedia);

        void Delete(ProductLoggingMedia productMedia);

        void HandleMain(ProductLoggingMedia productMedia);

        ProductLoggingMedia GetMainProductMedia(int productMediaId);

        void AddProductMedia(ProductLoggingMedia productMedia);
    }
}
