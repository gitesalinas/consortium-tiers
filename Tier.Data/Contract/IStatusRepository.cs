﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.Contract
{
    public interface IStatusRepository
    {
        StatusEntity Get(int id);
        IEnumerable<StatusEntity> GetAll(short status = (short)EnumStatus.None);
    }
}
