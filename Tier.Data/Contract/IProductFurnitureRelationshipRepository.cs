﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.Contract
{
    public interface IProductFurnitureRelationshipRepository
    {
        ProductFurnitureRelationship Get(int id);

        IEnumerable<ProductFurnitureRelationship> GetAll(short status = (short)EnumStatus.None);

        IEnumerable<ProductFurnitureRelationship> GetAllByProductOwner(int productOwnerId, short status = (short)EnumStatus.None);

        ProductFurnitureRelationship GetByProductOwnerAndProductDependant(int productOwnerId, int productDependantId);

        void Save(ProductFurnitureRelationship productRelationship);

        void Delete(ProductFurnitureRelationship productRelationship);
    }
}
