﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.Contract
{
    public interface IProductLoggingRepository
    {
        IEnumerable<ProductLogging> GetAllFull(short status = (short)EnumStatus.None);
        IEnumerable<ProductLogging> GetAll(short status = (short)EnumStatus.None);

        ProductLogging GetFull(int id, short status = (short)EnumStatus.None);
        ProductLogging Get(int id, short status = (short)EnumStatus.None);

        void Save(ProductLogging product);

        void Delete(ProductLogging product);
    }
}
