namespace Tier.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RoomMediaImpl : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.room_media",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsMain = c.Boolean(nullable: false),
                        RoomId = c.Int(nullable: false),
                        ResourceId = c.Int(nullable: false),
                        Status = c.Short(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.resource", t => t.ResourceId, cascadeDelete: true)
                .ForeignKey("dbo.room", t => t.RoomId, cascadeDelete: true)
                .Index(t => t.RoomId)
                .Index(t => t.ResourceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.room_media", "RoomId", "dbo.room");
            DropForeignKey("dbo.room_media", "ResourceId", "dbo.resource");
            DropIndex("dbo.room_media", new[] { "ResourceId" });
            DropIndex("dbo.room_media", new[] { "RoomId" });
            DropTable("dbo.room_media");
        }
    }
}
