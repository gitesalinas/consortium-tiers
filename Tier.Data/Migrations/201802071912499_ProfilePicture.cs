namespace Tier.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProfilePicture : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.resource",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 150),
                        Extension = c.String(nullable: false, maxLength: 15),
                        ContentLength = c.Int(nullable: false),
                        ContentType = c.String(nullable: false, maxLength: 75),
                        PublicName = c.String(nullable: false, maxLength: 150),
                        DirectoryPath = c.String(maxLength: 500),
                        Path = c.String(maxLength: 750),
                        VirtualDirectoryPath = c.String(maxLength: 500),
                        VirtualPath = c.String(maxLength: 750),
                        ThumbnailDirectoryPath = c.String(maxLength: 500),
                        ThumbnailPath = c.String(maxLength: 750),
                        ThumbnailVirtualDirectoryPath = c.String(maxLength: 500),
                        ThumbnailVirtualPath = c.String(maxLength: 750),
                        Status = c.Short(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.user", "PictureId", c => c.Int());
            CreateIndex("dbo.user", "PictureId");
            AddForeignKey("dbo.user", "PictureId", "dbo.resource", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.user", "PictureId", "dbo.resource");
            DropIndex("dbo.user", new[] { "PictureId" });
            DropColumn("dbo.user", "PictureId");
            DropTable("dbo.resource");
        }
    }
}
