namespace Tier.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserTypeFunctionality : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.userType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 75),
                        Description = c.String(nullable: false, maxLength: 300),
                        Status = c.Short(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.user", "TypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.user", "TypeId");
            AddForeignKey("dbo.user", "TypeId", "dbo.userType", "Id", cascadeDelete: true);
            DropColumn("dbo.user", "Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.user", "Type", c => c.Short(nullable: false));
            DropForeignKey("dbo.user", "TypeId", "dbo.userType");
            DropIndex("dbo.user", new[] { "TypeId" });
            DropColumn("dbo.user", "TypeId");
            DropTable("dbo.userType");
        }
    }
}
