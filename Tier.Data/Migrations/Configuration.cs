namespace Tier.Data.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Tier.Data.DataAccess.Db.Orm;
    using Tier.Entity;

    public sealed class Configuration : DbMigrationsConfiguration<MainDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            //AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(MainDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            DateTime date = DateTime.Now;

            //SeedUserAndPerson(context, date);
            //SeedUserTypes(context, date);
            //SeedStatuses(context, date);
            //SeedProductTypes(context, date);
        }

        private void SeedProductTypes(MainDbContext context, DateTime date)
        {
            List<ProductType> productTypes = new List<ProductType>()
            {
                new ProductType() { Id = 1, Code = 1, Name = "Single", Description = "Single", Status = 1, CreatedDate = date },
                new ProductType() { Id = 2, Code = 2, Name = "Pack", Description = "Pack", Status = 1, CreatedDate = date }
            };

            productTypes.ForEach(pt => context.ProductTypes.AddOrUpdate(i => i.Id, pt));
        }

        private void SeedUserAndPerson(MainDbContext context, DateTime date)
        {
            Person person = new Person()
            {
                Id = 1,
                Firstname = "Eder",
                Lastname = "Salinas",
                Email = "esalinas0686@gmail.com",
                Gender = "M",
                Birth = new DateTime(1986, 6, 20),
                Status = (Int16)1,
                CreatedBy = 0,
                CreatedDate = date,

                Facebook = "https://www.facebook.com/edercach",
                GooglePlus = "",
                Linkedin = "https://www.linkedin.com/in/esalinas86/",
                Skype = "edersalinas86",
                Website = "",
                WorkPhone = "269551",
                HomePhone = "509314",
                MobilPhone = "991891018",
            };

            context.Persons.AddOrUpdate(p => p.Firstname, person);

            User user = new User()
            {
                Id = 1,
                Username = "esalinas",
                TypeId = 1,
                Password = "123456",
                Status = (Int16)1,
                Person = person,
                CreatedBy = 0,
                CreatedDate = date
            };

            context.Users.AddOrUpdate(u => u.Username, user);
        }

        private void SeedUserTypes(MainDbContext context, DateTime date)
        {
            List<UserType> userTypes = new List<UserType>()
            {
                new UserType() { Id = 1, Code = 1, Name = "Super User", Description = "Super User Type", Status = 1, CreatedDate = date },
                new UserType() { Id = 2, Code = 2, Name = "Admin", Description = "Admin Type", Status = 1, CreatedDate = date },
                new UserType() { Id = 3, Code = 3, Name = "User", Description = "User Type", Status = 1, CreatedDate = date },
                new UserType() { Id = 4, Code = 4, Name = "Client", Description = "Client Type", Status = 1, CreatedDate = date }
            };

            userTypes.ForEach(ut => context.UserTypes.AddOrUpdate(utc => utc.Id, ut));
        }

        private void SeedStatuses(MainDbContext context, DateTime date)
        {
            List<StatusEntity> statuses = new List<StatusEntity>()
            {
                new StatusEntity() { Id = 1, Code = -1, Name = "None", Description = "None", Status = 1, CreatedDate = date },
                new StatusEntity() { Id = 2, Code = 0, Name = "Disabled", Description = "Disabled", Status = 1, CreatedDate = date },
                new StatusEntity() { Id = 3, Code = 1, Name = "Enabled", Description = "Enabled", Status = 1, CreatedDate = date },
                new StatusEntity() { Id = 4, Code = 2, Name = "Deleted", Description = "Deleted", Status = 1, CreatedDate = date }
            };

            statuses.ForEach(s => context.Statuses.AddOrUpdate(sc => sc.Id, s));
        }
    }
}
