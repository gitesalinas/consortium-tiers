namespace Tier.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RoomsFacilities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.facility",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 75),
                        Description = c.String(nullable: false, maxLength: 300),
                        Status = c.Short(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.room",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 75),
                        Description = c.String(nullable: false, maxLength: 250),
                        FullDescription = c.String(nullable: false, storeType: "ntext"),
                        Price = c.Decimal(nullable: false, precision: 10, scale: 2),
                        Status = c.Short(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.room_facility",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoomId = c.Int(nullable: false),
                        FacilityId = c.Int(nullable: false),
                        Status = c.Short(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.facility", t => t.FacilityId, cascadeDelete: true)
                .ForeignKey("dbo.room", t => t.RoomId, cascadeDelete: true)
                .Index(t => t.RoomId)
                .Index(t => t.FacilityId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.room_facility", "RoomId", "dbo.room");
            DropForeignKey("dbo.room_facility", "FacilityId", "dbo.facility");
            DropIndex("dbo.room_facility", new[] { "FacilityId" });
            DropIndex("dbo.room_facility", new[] { "RoomId" });
            DropTable("dbo.room_facility");
            DropTable("dbo.room");
            DropTable("dbo.facility");
        }
    }
}
