namespace Tier.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Profile_Birth : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.person", "Birth", c => c.DateTime(nullable: false));
            AlterColumn("dbo.person", "HomePhone", c => c.String(maxLength: 50));
            AlterColumn("dbo.person", "WorkPhone", c => c.String(maxLength: 50));
            AlterColumn("dbo.person", "MobilPhone", c => c.String(maxLength: 50));
            AlterColumn("dbo.person", "Website", c => c.String(maxLength: 500));
            AlterColumn("dbo.person", "Facebook", c => c.String(maxLength: 500));
            AlterColumn("dbo.person", "Twitter", c => c.String(maxLength: 500));
            AlterColumn("dbo.person", "GooglePlus", c => c.String(maxLength: 500));
            AlterColumn("dbo.person", "Linkedin", c => c.String(maxLength: 500));
            AlterColumn("dbo.person", "Skype", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.person", "Skype", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.person", "Linkedin", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.person", "GooglePlus", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.person", "Twitter", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.person", "Facebook", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.person", "Website", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.person", "MobilPhone", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.person", "WorkPhone", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.person", "HomePhone", c => c.String(nullable: false, maxLength: 50));
            DropColumn("dbo.person", "Birth");
        }
    }
}
