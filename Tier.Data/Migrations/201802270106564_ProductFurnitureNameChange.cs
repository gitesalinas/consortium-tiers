namespace Tier.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductFurnitureNameChange : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.product_forniture_media", newName: "product_furniture_media");
            RenameTable(name: "dbo.product_forniture", newName: "product_furniture");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.product_furniture", newName: "product_forniture");
            RenameTable(name: "dbo.product_furniture_media", newName: "product_forniture_media");
        }
    }
}
