namespace Tier.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductImpl : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.product", newName: "product_forniture");
            CreateTable(
                "dbo.product_logging",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SKU = c.String(nullable: false, maxLength: 15),
                        Name = c.String(),
                        Description = c.String(nullable: false, maxLength: 250),
                        FullDescription = c.String(storeType: "ntext"),
                        Stock = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Tax = c.Decimal(precision: 10, scale: 2),
                        UnitPrice = c.Decimal(nullable: false, precision: 10, scale: 2),
                        TotalPrice = c.Decimal(nullable: false, precision: 10, scale: 2),
                        Status = c.Short(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.product_forniture", "SKU", c => c.String(nullable: false, maxLength: 15));
            AlterColumn("dbo.product_forniture", "FullDescription", c => c.String(storeType: "ntext"));
            AlterColumn("dbo.product_forniture", "Tax", c => c.Decimal(precision: 10, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.product_forniture", "Tax", c => c.Decimal(nullable: false, precision: 10, scale: 2));
            AlterColumn("dbo.product_forniture", "FullDescription", c => c.String(nullable: false, storeType: "ntext"));
            AlterColumn("dbo.product_forniture", "SKU", c => c.String());
            DropTable("dbo.product_logging");
            RenameTable(name: "dbo.product_forniture", newName: "product");
        }
    }
}
