namespace Tier.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CodeColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.userType", "Code", c => c.Int(nullable: false));
            CreateIndex("dbo.userType", "Code", unique: true, name: "UserType_IX_Code");
        }
        
        public override void Down()
        {
            DropIndex("dbo.userType", new[] { "UserType_IX_Code" });
            DropColumn("dbo.userType", "Code");
        }
    }
}
