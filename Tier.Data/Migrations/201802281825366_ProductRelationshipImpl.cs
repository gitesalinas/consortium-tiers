namespace Tier.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class ProductRelationshipImpl : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.product_furniture_relationship",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    ProductOwnerId = c.Int(nullable: false),
                    ProductDependantId = c.Int(nullable: false),
                    SKU = c.String(),
                    Name = c.String(nullable: false, maxLength: 75),
                    Units = c.Decimal(nullable: false, precision: 10, scale: 2),
                    Tax = c.Decimal(nullable: false, precision: 10, scale: 2),
                    UnitPrice = c.Decimal(nullable: false, precision: 10, scale: 2),
                    TotalPrice = c.Decimal(nullable: false, precision: 10, scale: 2),
                    Status = c.Short(nullable: false),
                    CreatedBy = c.Int(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    UpdatedBy = c.Int(),
                    UpdatedDate = c.DateTime(),
                    //ProductFurniture_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.product_furniture", t => t.ProductDependantId)
                .ForeignKey("dbo.product_furniture", t => t.ProductOwnerId)
                //.ForeignKey("dbo.product_furniture", t => t.ProductFurniture_Id)
                .Index(t => t.ProductOwnerId)
                .Index(t => t.ProductDependantId);
            //.Index(t => t.ProductFurniture_Id);

            CreateTable(
                "dbo.product_logging_relationship",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    ProductOwnerId = c.Int(nullable: false),
                    ProductDependantId = c.Int(nullable: false),
                    SKU = c.String(),
                    Name = c.String(nullable: false, maxLength: 75),
                    Units = c.Decimal(nullable: false, precision: 10, scale: 2),
                    Tax = c.Decimal(nullable: false, precision: 10, scale: 2),
                    UnitPrice = c.Decimal(nullable: false, precision: 10, scale: 2),
                    TotalPrice = c.Decimal(nullable: false, precision: 10, scale: 2),
                    Status = c.Short(nullable: false),
                    CreatedBy = c.Int(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    UpdatedBy = c.Int(),
                    UpdatedDate = c.DateTime(),
                    //ProductLogging_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.product_logging", t => t.ProductDependantId)
                .ForeignKey("dbo.product_logging", t => t.ProductOwnerId)
                //.ForeignKey("dbo.product_logging", t => t.ProductLogging_Id)
                .Index(t => t.ProductOwnerId)
                .Index(t => t.ProductDependantId);
                //.Index(t => t.ProductLogging_Id);

            AlterColumn("dbo.product_furniture", "Name", c => c.String(nullable: false, maxLength: 75));
        }

        public override void Down()
        {
            //DropForeignKey("dbo.product_logging_relationship", "ProductLogging_Id", "dbo.product_logging");
            DropForeignKey("dbo.product_logging_relationship", "ProductOwnerId", "dbo.product_logging");
            DropForeignKey("dbo.product_logging_relationship", "ProductDependantId", "dbo.product_logging");
            //DropForeignKey("dbo.product_furniture_relationship", "ProductFurniture_Id", "dbo.product_furniture");
            DropForeignKey("dbo.product_furniture_relationship", "ProductOwnerId", "dbo.product_furniture");
            DropForeignKey("dbo.product_furniture_relationship", "ProductDependantId", "dbo.product_furniture");
            //DropIndex("dbo.product_logging_relationship", new[] { "ProductLogging_Id" });
            DropIndex("dbo.product_logging_relationship", new[] { "ProductDependantId" });
            DropIndex("dbo.product_logging_relationship", new[] { "ProductOwnerId" });
            //DropIndex("dbo.product_furniture_relationship", new[] { "ProductFurniture_Id" });
            DropIndex("dbo.product_furniture_relationship", new[] { "ProductDependantId" });
            DropIndex("dbo.product_furniture_relationship", new[] { "ProductOwnerId" });
            AlterColumn("dbo.product_furniture", "Name", c => c.String());
            DropTable("dbo.product_logging_relationship");
            DropTable("dbo.product_furniture_relationship");
        }
    }
}
