namespace Tier.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Profile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.person", "HomePhone", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.person", "WorkPhone", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.person", "MobilPhone", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.person", "Website", c => c.String(nullable: false, maxLength: 500));
            AddColumn("dbo.person", "Facebook", c => c.String(nullable: false, maxLength: 500));
            AddColumn("dbo.person", "Twitter", c => c.String(nullable: false, maxLength: 500));
            AddColumn("dbo.person", "GooglePlus", c => c.String(nullable: false, maxLength: 500));
            AddColumn("dbo.person", "Linkedin", c => c.String(nullable: false, maxLength: 500));
            AddColumn("dbo.person", "Skype", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.person", "Skype");
            DropColumn("dbo.person", "Linkedin");
            DropColumn("dbo.person", "GooglePlus");
            DropColumn("dbo.person", "Twitter");
            DropColumn("dbo.person", "Facebook");
            DropColumn("dbo.person", "Website");
            DropColumn("dbo.person", "MobilPhone");
            DropColumn("dbo.person", "WorkPhone");
            DropColumn("dbo.person", "HomePhone");
        }
    }
}
