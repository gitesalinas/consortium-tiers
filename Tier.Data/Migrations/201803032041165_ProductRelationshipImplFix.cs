namespace Tier.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductRelationshipImplFix : DbMigration
    {
        public override void Up()
        {
            //DropIndex("dbo.product_furniture_relationship", new[] { "ProductOwnerId" });
            //DropIndex("dbo.product_furniture_relationship", new[] { "ProductFurniture_Id" });
            //DropIndex("dbo.product_logging_relationship", new[] { "ProductOwnerId" });
            //DropIndex("dbo.product_logging_relationship", new[] { "ProductLogging_Id" });
            //DropColumn("dbo.product_furniture_relationship", "ProductOwnerId");
            //DropColumn("dbo.product_logging_relationship", "ProductOwnerId");
            //RenameColumn(table: "dbo.product_furniture_relationship", name: "ProductFurniture_Id", newName: "ProductOwnerId");
            //RenameColumn(table: "dbo.product_logging_relationship", name: "ProductLogging_Id", newName: "ProductOwnerId");
            //AlterColumn("dbo.product_furniture_relationship", "ProductOwnerId", c => c.Int(nullable: false));
            //AlterColumn("dbo.product_logging_relationship", "ProductOwnerId", c => c.Int(nullable: false));
            //CreateIndex("dbo.product_furniture_relationship", "ProductOwnerId");
            //CreateIndex("dbo.product_logging_relationship", "ProductOwnerId");
        }
        
        public override void Down()
        {
            //DropIndex("dbo.product_logging_relationship", new[] { "ProductOwnerId" });
            //DropIndex("dbo.product_furniture_relationship", new[] { "ProductOwnerId" });
            //AlterColumn("dbo.product_logging_relationship", "ProductOwnerId", c => c.Int());
            //AlterColumn("dbo.product_furniture_relationship", "ProductOwnerId", c => c.Int());
            //RenameColumn(table: "dbo.product_logging_relationship", name: "ProductOwnerId", newName: "ProductLogging_Id");
            //RenameColumn(table: "dbo.product_furniture_relationship", name: "ProductOwnerId", newName: "ProductFurniture_Id");
            //AddColumn("dbo.product_logging_relationship", "ProductOwnerId", c => c.Int(nullable: false));
            //AddColumn("dbo.product_furniture_relationship", "ProductOwnerId", c => c.Int(nullable: false));
            //CreateIndex("dbo.product_logging_relationship", "ProductLogging_Id");
            //CreateIndex("dbo.product_logging_relationship", "ProductOwnerId");
            //CreateIndex("dbo.product_furniture_relationship", "ProductFurniture_Id");
            //CreateIndex("dbo.product_furniture_relationship", "ProductOwnerId");
        }
    }
}
