namespace Tier.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductMediaImpl : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.product_forniture_media",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsMain = c.Boolean(nullable: false),
                        ProductId = c.Int(nullable: false),
                        ResourceId = c.Int(nullable: false),
                        Status = c.Short(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.product_forniture", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.resource", t => t.ResourceId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.ResourceId);
            
            CreateTable(
                "dbo.product_logging_media",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsMain = c.Boolean(nullable: false),
                        ProductId = c.Int(nullable: false),
                        ResourceId = c.Int(nullable: false),
                        Status = c.Short(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.product_logging", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.resource", t => t.ResourceId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.ResourceId);
            
            CreateTable(
                "dbo.product_type",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 75),
                        Description = c.String(nullable: false, maxLength: 300),
                        Status = c.Short(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.product_logging_media", "ResourceId", "dbo.resource");
            DropForeignKey("dbo.product_logging_media", "ProductId", "dbo.product_logging");
            DropForeignKey("dbo.product_forniture_media", "ResourceId", "dbo.resource");
            DropForeignKey("dbo.product_forniture_media", "ProductId", "dbo.product_forniture");
            DropIndex("dbo.product_type", new[] { "Code" });
            DropIndex("dbo.product_logging_media", new[] { "ResourceId" });
            DropIndex("dbo.product_logging_media", new[] { "ProductId" });
            DropIndex("dbo.product_forniture_media", new[] { "ResourceId" });
            DropIndex("dbo.product_forniture_media", new[] { "ProductId" });
            DropTable("dbo.product_type");
            DropTable("dbo.product_logging_media");
            DropTable("dbo.product_forniture_media");
        }
    }
}
