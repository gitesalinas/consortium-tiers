namespace Tier.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StatusImpl : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.status",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 75),
                        Description = c.String(nullable: false, maxLength: 300),
                        Status = c.Short(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code, unique: true, name: "Status_IX_Code");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.status", new[] { "Status_IX_Code" });
            DropTable("dbo.status");
        }
    }
}
