namespace Tier.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductTypeImpl : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.product_type");
            AddColumn("dbo.product_furniture", "TypeId", c => c.Int(nullable: true));
            AddColumn("dbo.product_logging", "TypeId", c => c.Int(nullable: true));
            AddPrimaryKey("dbo.product_type", "Code");
            CreateIndex("dbo.product_furniture", "TypeId");
            CreateIndex("dbo.product_logging", "TypeId");
            AddForeignKey("dbo.product_furniture", "TypeId", "dbo.product_type", "Code", cascadeDelete: true);
            AddForeignKey("dbo.product_logging", "TypeId", "dbo.product_type", "Code", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.product_logging", "TypeId", "dbo.product_type");
            DropForeignKey("dbo.product_furniture", "TypeId", "dbo.product_type");
            DropIndex("dbo.product_logging", new[] { "TypeId" });
            DropIndex("dbo.product_furniture", new[] { "TypeId" });
            DropPrimaryKey("dbo.product_type");
            DropColumn("dbo.product_logging", "TypeId");
            DropColumn("dbo.product_furniture", "TypeId");
            AddPrimaryKey("dbo.product_type", "Id");
        }
    }
}
