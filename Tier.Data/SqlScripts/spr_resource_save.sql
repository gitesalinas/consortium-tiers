create procedure [dbo].[spr_resource_save]
@id int,
@name nvarchar(150),
@extension nvarchar(15),
@contentLength int,
@contentType nvarchar(75),
@publicName nvarchar(150),
@directoryPath nvarchar(500),
@path nvarchar(750),
@virtualDirectoryPath nvarchar(500),  
@virtualPath nvarchar(750), 
@thumbnailDirectoryPath nvarchar(500), 
@thumbnailPath nvarchar(750),
@thumbnailVirtualDirectoryPath nvarchar(500),
@thumbnailVirtualPath nvarchar(750),
@createdBy int,
@createdDate datetime,
@status smallint,
@newId int output
as
	begin
		
		if @id = 0
			begin
				insert into [resource]
				(	name, extension, contentLength, contentType, publicName, directoryPath, 
					[path], virtualDirectoryPath, virtualPath, thumbnailDirectoryPath, thumbnailPath, 
					thumbnailVirtualDirectoryPath, thumbnailVirtualPath, createdBy, createdDate, [status]
				)
				values
				(	@name, @extension, @contentLength, @contentType, @publicName, @directoryPath, 
					@path, @virtualDirectoryPath, @virtualPath, @thumbnailDirectoryPath, @thumbnailPath, 
					@thumbnailVirtualDirectoryPath, @thumbnailVirtualPath, @createdBy, @createdDate, @status
				)
				
				set @newId = scope_identity();
			end
		else
			begin
				update [resource]
					set
						name = @name,
						extension = @extension, 
						contentLength = @contentLength,
						contentType = @contentType, 
						publicName = @publicName, 
						directoryPath = @directoryPath, 
						[path] = @path, 
						virtualDirectoryPath = @virtualDirectoryPath, 
						virtualPath = @virtualPath, 
						thumbnailDirectoryPath = @thumbnailDirectoryPath, 
						thumbnailPath = @thumbnailPath, 
						thumbnailVirtualDirectoryPath = @thumbnailVirtualDirectoryPath, 
						thumbnailVirtualPath = @thumbnailVirtualPath, 
						updatedBy = @createdBy, 
						updatedDate = @createdDate, 
						[status] = @status
					where id = @id
				
				set @newId = @id
			end
	end