create procedure spr_user_status
@id int,
@status smallint
as
	begin transaction
		begin try
			
			declare @personId int = (select personId from [user] where id = @id)
			declare @pictureId int = (select PictureId from [user] where id = @id)
			
			update [user]
				set
					[status] = @status
				where id = @id
				
			update person
				set
					[status] = @status
				where id = @personId
			
			if @pictureId is not null
				begin
					update [resource]
						set
							[status] = @status
						where id = @pictureId
				end
		
			if @@TRANCOUNT > 0
				commit transaction;
		end try
		begin catch
			if @@TRANCOUNT > 0
				rollback transaction;
		end catch