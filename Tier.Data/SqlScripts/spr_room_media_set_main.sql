alter procedure spr_room_media_set_main
@Id int,
@roomId int,
@isMain bit,
@updatedBy int,
@updatedDate datetime
as
	begin transaction
		begin try
			
			update room_media
				set
					IsMain = 0,
					UpdatedBy = @updatedBy,
					UpdatedDate = @updatedDate
				where RoomId = @roomId and [Status] != 2
			
			update room_media
				set
					IsMain = ~@isMain,
					UpdatedBy = @updatedBy,
					UpdatedDate = @updatedDate
				where Id = @Id
		
			if @@TRANCOUNT > 0
				commit transaction;
		end try
		begin catch
			if @@TRANCOUNT > 0
				rollback transaction;
		end catch