create procedure spr_room_media_save
@id int,
@isMain bit,
@roomId int,
@resourceId int,
@createdBy int,
@createdDate datetime,
@status smallint,
@newId int output
as
	begin
	
	
		if @id = 0
			begin
				insert into room_media
				(	
					IsMain, RoomId, ResourceId, [Status], CreatedBy, CreatedDate
				)
				values
				(
					@isMain, @roomId, @resourceId, @status, @createdBy, @createdDate
				)
				
				set @newId = scope_identity();
			end
		else
			begin
				update room_media
					set
						IsMain = @isMain,
						RoomId = @roomId, 
						ResourceId = @resourceId,
						[Status] = @status,
						UpdatedBy = @createdBy,
						UpdatedDate = @createdDate
					where Id = @id
				
				set @newId = @id;
			end
	end