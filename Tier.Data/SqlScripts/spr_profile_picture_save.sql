create procedure [dbo].[spr_profile_picture_save]
@userId int, 
@resourceId int, 
@name nvarchar(150),
@extension nvarchar(15),
@contentLength int,
@contentType nvarchar(75),
@publicName nvarchar(150),
@directoryPath nvarchar(500),
@path nvarchar(750),
@virtualDirectoryPath nvarchar(500),  
@virtualPath nvarchar(750), 
@thumbnailDirectoryPath nvarchar(500), 
@thumbnailPath nvarchar(750),
@thumbnailVirtualDirectoryPath nvarchar(500),
@thumbnailVirtualPath nvarchar(750),
@createdBy int,
@createdDate datetime,
@status smallint,
@newId int output
as
	begin transaction
		begin try
			
			exec spr_resource_save @resourceId, @name, @extension, @contentLength, @contentType, 
				@publicName, @directoryPath, @path, @virtualDirectoryPath, @virtualPath, 
				@thumbnailDirectoryPath, @thumbnailPath, @thumbnailVirtualDirectoryPath, 
				@thumbnailVirtualPath, @createdBy, @createdDate, @status, @newId output;
			
			update [user]
				set
					PictureId = @newId
				where id = @userId;
			
		
			if @@TRANCOUNT > 0
				commit transaction;
		end try
		begin catch
			if @@TRANCOUNT > 0
				rollback transaction;
		end catch