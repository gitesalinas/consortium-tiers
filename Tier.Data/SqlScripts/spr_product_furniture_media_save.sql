create procedure spr_product_furniture_media_save
@id int,
@isMain bit,
@productId int,
@resourceId int,
@createdBy int,
@createdDate datetime,
@status smallint,
@newId int output
as
	begin
	
	
		if @id = 0
			begin
				insert into product_furniture_media
				(	
					IsMain, ProductId, ResourceId, [Status], CreatedBy, CreatedDate
				)
				values
				(
					@isMain, @productId, @resourceId, @status, @createdBy, @createdDate
				)
				
				set @newId = scope_identity();
			end
		else
			begin
				update product_furniture_media
					set
						IsMain = @isMain,
						ProductId = @productId, 
						ResourceId = @resourceId,
						[Status] = @status,
						UpdatedBy = @createdBy,
						UpdatedDate = @createdDate
					where Id = @id
				
				set @newId = @id;
			end
	end