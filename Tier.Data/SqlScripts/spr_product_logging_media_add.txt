create procedure spr_product_logging_media_add
@productId int,
@isMain bit,
@name nvarchar(150),
@extension nvarchar(15),
@contentLength int,
@contentType nvarchar(75),
@publicName nvarchar(150),
@directoryPath nvarchar(500),
@path nvarchar(750),
@virtualDirectoryPath nvarchar(500),  
@virtualPath nvarchar(750), 
@thumbnailDirectoryPath nvarchar(500), 
@thumbnailPath nvarchar(750),
@thumbnailVirtualDirectoryPath nvarchar(500),
@thumbnailVirtualPath nvarchar(750),
@createdBy int,
@createdDate datetime,
@status smallint,
@newResourceId int output,
@newProductMediaId int output
as
	begin transaction
		begin try
			
			exec spr_resource_save 0, @name, @extension, @contentLength, @contentType, 
				@publicName, @directoryPath, @path, @virtualDirectoryPath, @virtualPath, 
				@thumbnailDirectoryPath, @thumbnailPath, @thumbnailVirtualDirectoryPath, 
				@thumbnailVirtualPath, @createdBy, @createdDate, @status, @newResourceId output;
				
			exec spr_product_logging_media_save 0, @isMain, @productId, @newResourceId, @createdBy, @createdDate, @status, @newProductMediaId output;
				
		
			if @@TRANCOUNT > 0
				commit transaction;
		end try
		begin catch
			if @@TRANCOUNT > 0
				rollback transaction;
		end catch