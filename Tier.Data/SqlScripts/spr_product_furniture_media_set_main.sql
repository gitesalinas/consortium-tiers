create procedure spr_product_furniture_media_set_main
@Id int,
@productId int,
@isMain bit,
@updatedBy int,
@updatedDate datetime
as
	begin transaction
		begin try
			
			update product_furniture_media
				set
					IsMain = 0,
					UpdatedBy = @updatedBy,
					UpdatedDate = @updatedDate
				where ProductId = @productId and [Status] != 2
			
			update product_furniture_media
				set
					IsMain = ~@isMain,
					UpdatedBy = @updatedBy,
					UpdatedDate = @updatedDate
				where Id = @Id
		
			if @@TRANCOUNT > 0
				commit transaction;
		end try
		begin catch
			if @@TRANCOUNT > 0
				rollback transaction;
		end catch