﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Contract
{
    public interface IUserRepository : Tier.Data.Contract.IUserRepository, IRepository<User>
    {
        //User GetProfile(Expression<Func<User, bool>> predicate);
        //void SaveProfile(User user);
    }
}
