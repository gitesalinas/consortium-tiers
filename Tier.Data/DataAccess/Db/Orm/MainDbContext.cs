﻿using SqlProviderServices = System.Data.Entity.SqlServer.SqlProviderServices;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.DataAccess.Db.Orm.Configuration;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm
{
    public class MainDbContext : DbContext
    {
        public MainDbContext() : base("Consortium")
        {
            //Database.SetInitializer<MainDbContext>(new Initializer());
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<ProductLogging> ProductsLogging { get; set; }
        public virtual DbSet<ProductFurniture> ProductsFurniture { get; set; }
        public virtual DbSet<Resource> Resources { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }
        public virtual DbSet<StatusEntity> Statuses { get; set; }
        public virtual DbSet<Facility> Facilities { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<RoomFacility> RoomsFacilities { get; set; }
        public virtual DbSet<RoomMedia> RoomsMedias { get; set; }

        public virtual DbSet<ProductType> ProductTypes { get; set; }
        public virtual DbSet<ProductFurnitureMedia> ProductFurnitureMedias { get; set; }
        public virtual DbSet<ProductLoggingMedia> ProductLoggingMedias { get; set; }

        public virtual DbSet<ProductFurnitureRelationship> ProductFurnitureRelationships { get; set; }
        public virtual DbSet<ProductLoggingRelationship> ProductLoggingRelationships { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new PersonConfiguration());

            modelBuilder.Configurations.Add(new ProductFurnitureConfiguration());
            modelBuilder.Configurations.Add(new ProductLoggingConfiguration());

            modelBuilder.Configurations.Add(new ResourceConfiguration());
            modelBuilder.Configurations.Add(new UserTypeConfiguration());
            modelBuilder.Configurations.Add(new StatusConfiguration());
            modelBuilder.Configurations.Add(new FacilityConfiguration());
            modelBuilder.Configurations.Add(new RoomConfiguration());
            modelBuilder.Configurations.Add(new RoomFacilityConfiguration());
            modelBuilder.Configurations.Add(new RoomMediaConfiguration());

            modelBuilder.Configurations.Add(new ProductTypeConfiguration());
            modelBuilder.Configurations.Add(new ProductFurnitureMediaConfiguration());
            modelBuilder.Configurations.Add(new ProductLoggingMediaConfiguration());

            modelBuilder.Configurations.Add(new ProductFurnitureRelationshipConfiguration());
            modelBuilder.Configurations.Add(new ProductLoggingRelationshipConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        private class Initializer : DropCreateDatabaseAlways<MainDbContext>//DropCreateDatabaseIfModelChanges<MainDbContext>
        {
            protected override void Seed(MainDbContext context)
            {
                base.Seed(context);

                Person person = new Person()
                {
                    Firstname = "Eder",
                    Lastname = "Salinas",
                    Email = "esalinas0686@gmail.com",
                    Gender = "M",
                    Status = (Int16)1,
                    CreatedBy = 0,
                    CreatedDate = DateTime.Now
                };

                User user = new User()
                {
                    Username = "esalinas",
                    Password = "123456",
                    Status = (Int16)1,
                    /*Person = new Person()
                    {
                        Firstname = "Eder",
                        Lastname = "Salinas",
                        Email = "esalinas0686@gmail.com",
                        DocumentNumber = "44218438",
                        Status = (Int16)1,
                        CreatedBy = 0,
                        CreatedDate = DateTime.Now
                    },*/
                    CreatedBy = 0,
                    CreatedDate = DateTime.Now
                };
                person.Users = new List<User>();
                person.Users.Add(user);

                context.Persons.Add(person);
            }
        }
    }
}
