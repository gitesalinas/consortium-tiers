﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class ProductLoggingConfiguration : AbstractEntityTypeConfiguration<ProductLogging>
    {
        public ProductLoggingConfiguration() : base("product_logging")
        {
            //Map(m => m.MapInheritedProperties());
            Property(p => p.SKU).IsRequired().HasMaxLength(15);
            Property(p => p.Description).IsRequired().HasMaxLength(250);
            Property(p => p.FullDescription).IsOptional().HasColumnType("ntext");
            Property(p => p.Tax).IsOptional().HasColumnType("decimal").HasPrecision(10, 2);
            Property(p => p.UnitPrice).IsRequired().HasColumnType("decimal").HasPrecision(10, 2);
            Property(p => p.TotalPrice).IsRequired().HasColumnType("decimal").HasPrecision(10, 2);

            HasRequired(p => p.Type).WithMany().HasForeignKey(p => p.TypeId);
        }
    }
}
