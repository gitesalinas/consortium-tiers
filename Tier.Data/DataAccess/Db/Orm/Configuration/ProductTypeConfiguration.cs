﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class ProductTypeConfiguration : AbstractEntityTypeConfiguration<ProductType>
    {
        public ProductTypeConfiguration() : base("product_type")
        {
            HasKey(pt => pt.Code);
            Property(pt => pt.Code).IsUnique();
            Property(pt => pt.Name).IsRequired().HasMaxLength(75);
            Property(pt => pt.Description).IsRequired().HasMaxLength(300);
        }
    }
}
