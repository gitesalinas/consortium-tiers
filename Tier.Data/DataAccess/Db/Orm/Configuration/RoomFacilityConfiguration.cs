﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class RoomFacilityConfiguration : AbstractEntityTypeConfiguration<RoomFacility>
    {
        public RoomFacilityConfiguration() : base("room_facility")
        {
            //Property(rf => rf.Name).IsRequired().HasMaxLength(75);
            //Property(rf => rf.Description).IsRequired().HasMaxLength(300);
            HasRequired(rf => rf.Room).WithMany(r => r.RoomFacilities).HasForeignKey(rf => rf.RoomId);
            HasRequired(rf => rf.Facility).WithMany().HasForeignKey(rf => rf.FacilityId);
        }
    }
}
