﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class StatusConfiguration : AbstractEntityTypeConfiguration<StatusEntity>
    {
        public StatusConfiguration() : base("status")
        {
            Property(u => u.Code).IsUnique();
            Property(u => u.Name).IsRequired().HasMaxLength(75);
            Property(u => u.Description).IsRequired().HasMaxLength(300);
        }
    }
}
