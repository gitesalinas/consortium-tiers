﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class RoomMediaConfiguration : AbstractEntityTypeConfiguration<RoomMedia>
    {
        public RoomMediaConfiguration() : base("room_media")
        {
            HasRequired(rf => rf.Room).WithMany(r => r.RoomMedias).HasForeignKey(rf => rf.RoomId);
            HasRequired(rf => rf.Resource).WithMany().HasForeignKey(rf => rf.ResourceId);
        }
    }
}
