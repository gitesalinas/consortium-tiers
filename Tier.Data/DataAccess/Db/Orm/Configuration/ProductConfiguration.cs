﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class ProductConfiguration : AbstractEntityTypeConfiguration<BaseProduct>
    {
        public ProductConfiguration() : base("product")
        {
            Property(p => p.Description).IsRequired().HasMaxLength(250);
            Property(p => p.FullDescription).IsRequired().HasColumnType("ntext");
            Property(p => p.Tax).IsRequired().HasColumnType("decimal").HasPrecision(10, 2);
            Property(p => p.UnitPrice).IsRequired().HasColumnType("decimal").HasPrecision(10, 2);
            Property(p => p.TotalPrice).IsRequired().HasColumnType("decimal").HasPrecision(10, 2);
        }
    }
}
