﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class ProductFurnitureMediaConfiguration : AbstractEntityTypeConfiguration<ProductFurnitureMedia>
    {
        public ProductFurnitureMediaConfiguration() : base("product_furniture_media")
        {
            HasRequired(rf => rf.Product).WithMany(r => r.ProductMedias).HasForeignKey(rf => rf.ProductId);
            HasRequired(rf => rf.Resource).WithMany().HasForeignKey(rf => rf.ResourceId);
        }
    }
}
