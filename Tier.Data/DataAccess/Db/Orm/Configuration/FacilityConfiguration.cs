﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class FacilityConfiguration : AbstractEntityTypeConfiguration<Facility>
    {
        public FacilityConfiguration() : base("facility")
        {
            Property(f => f.Name).IsRequired().HasMaxLength(75);
            Property(f => f.Description).IsRequired().HasMaxLength(300);
        }
    }
}
