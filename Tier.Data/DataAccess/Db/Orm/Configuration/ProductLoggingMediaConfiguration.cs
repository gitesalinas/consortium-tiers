﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class ProductLoggingMediaConfiguration : AbstractEntityTypeConfiguration<ProductLoggingMedia>
    {
        public ProductLoggingMediaConfiguration() : base("product_logging_media")
        {
            HasRequired(rf => rf.Product).WithMany(r => r.ProductMedias).HasForeignKey(rf => rf.ProductId);
            HasRequired(rf => rf.Resource).WithMany().HasForeignKey(rf => rf.ResourceId);
        }
    }
}
