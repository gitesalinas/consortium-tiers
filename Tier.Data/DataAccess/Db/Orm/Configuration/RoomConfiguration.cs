﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class RoomConfiguration : AbstractEntityTypeConfiguration<Room>
    {
        public RoomConfiguration() : base("room")
        {
            Property(r => r.Name).IsRequired().HasMaxLength(75);
            Property(r => r.Description).IsRequired().HasMaxLength(250);
            Property(r => r.FullDescription).IsRequired().HasColumnType("ntext");
            Property(r => r.Price).IsRequired().HasColumnType("decimal").HasPrecision(10, 2);
            HasMany(r => r.RoomFacilities).WithRequired(rf => rf.Room);
        }
    }
}
