﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class ProductLoggingRelationshipConfiguration : AbstractEntityTypeConfiguration<ProductLoggingRelationship>
    {
        public ProductLoggingRelationshipConfiguration() : base("product_logging_relationship")
        {
            HasRequired(pr => pr.ProductOwner).WithMany(p => p.ProductRelationship).HasForeignKey(pr => pr.ProductOwnerId).WillCascadeOnDelete(false);
            HasRequired(pr => pr.ProductDependant).WithMany().HasForeignKey(pr => pr.ProductDependantId).WillCascadeOnDelete(false);

            Property(pr => pr.Name).IsRequired().HasMaxLength(75);
            Property(pr => pr.Units).IsRequired().HasColumnType("decimal").HasPrecision(10, 2);
            Property(pr => pr.Tax).IsRequired().HasColumnType("decimal").HasPrecision(10, 2);
            Property(pr => pr.UnitPrice).IsRequired().HasColumnType("decimal").HasPrecision(10, 2);
            Property(pr => pr.TotalPrice).IsRequired().HasColumnType("decimal").HasPrecision(10, 2);
        }
    }
}
