﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class UserTypeConfiguration : AbstractEntityTypeConfiguration<UserType>
    {
        public UserTypeConfiguration() : base("userType")
        {
            Property(ut => ut.Code).IsUnique();
            Property(ut => ut.Name).IsRequired().HasMaxLength(75);
            Property(ut => ut.Description).IsRequired().HasMaxLength(300);
        }
    }
}
