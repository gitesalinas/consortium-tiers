﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class ResourceConfiguration : AbstractEntityTypeConfiguration<Resource>
    {
        public ResourceConfiguration() : base("resource")
        {
            Property(r => r.Name).IsRequired().HasMaxLength(150);
            Property(r => r.Extension).IsRequired().HasMaxLength(15);
            Property(r => r.ContentLength).IsRequired();
            Property(r => r.ContentType).IsRequired().HasMaxLength(75);
            Property(r => r.PublicName).IsRequired().HasMaxLength(150);
            Property(r => r.DirectoryPath).HasMaxLength(500);
            Property(r => r.Path).HasMaxLength(750);
            Property(r => r.VirtualDirectoryPath).HasMaxLength(500);
            Property(r => r.VirtualPath).HasMaxLength(750);
            Property(r => r.ThumbnailDirectoryPath).HasMaxLength(500);
            Property(r => r.ThumbnailPath).HasMaxLength(750);
            Property(r => r.ThumbnailVirtualDirectoryPath).HasMaxLength(500);
            Property(r => r.ThumbnailVirtualPath).HasMaxLength(750);
        }
    }
}
