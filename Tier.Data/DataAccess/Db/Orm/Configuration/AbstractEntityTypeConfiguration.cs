﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class AbstractEntityTypeConfiguration<TEntity> : EntityTypeConfiguration<TEntity> where TEntity : BaseEntity
    {
        public AbstractEntityTypeConfiguration(string tableName)
        {
            ToTable(tableName);

            HasKey(b => b.Id);

            Property(b => b.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(b => b.Status).IsRequired().HasColumnType("smallint");
            Property(b => b.CreatedBy).IsRequired();
            Property(b => b.CreatedDate).IsRequired();
            Property(b => b.UpdatedBy).IsOptional();
            Property(b => b.UpdatedDate).IsOptional();
        }
    }

    public static class EntityTypeConfigurationExtension
    {
        public static PrimitivePropertyConfiguration IsUnique(this PrimitivePropertyConfiguration configuration)
        {
            return configuration.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));
        }
    }

}
