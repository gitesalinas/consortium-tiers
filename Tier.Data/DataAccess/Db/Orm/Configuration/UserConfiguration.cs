﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class UserConfiguration : AbstractEntityTypeConfiguration<User>
    {
        public UserConfiguration() : base("user")
        {
            Property(u => u.Username).IsRequired().HasMaxLength(20);
            Property(u => u.Password).IsRequired().HasMaxLength(16);
            HasRequired(u => u.Person).WithMany(p => p.Users).HasForeignKey(u => u.PersonId);
            HasOptional(u => u.Picture);//.WithMany().HasForeignKey(u => u.PictureId);//.WithRequired();
            HasRequired(u => u.Type);//.WithMany().HasForeignKey(u => u.TypeId);
        }
    }
}
