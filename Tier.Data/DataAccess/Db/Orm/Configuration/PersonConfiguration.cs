﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Db.Orm.Configuration
{
    public class PersonConfiguration : AbstractEntityTypeConfiguration<Person>
    {
        public PersonConfiguration() : base("person")
        {
            Property(p => p.Firstname).IsRequired().HasMaxLength(100);
            Property(p => p.Lastname).IsRequired().HasMaxLength(150);
            Property(p => p.Email).IsRequired().HasMaxLength(75);
            Property(p => p.Gender).IsRequired().HasMaxLength(1);
            Property(p => p.Birth).IsRequired().HasColumnType("datetime");

            Property(p => p.HomePhone).HasMaxLength(50);
            Property(p => p.WorkPhone).HasMaxLength(50);
            Property(p => p.MobilPhone).HasMaxLength(50);
            Property(p => p.Website).HasMaxLength(500);
            Property(p => p.Facebook).HasMaxLength(500);
            Property(p => p.Twitter).HasMaxLength(500);
            Property(p => p.GooglePlus).HasMaxLength(500);
            Property(p => p.Linkedin).HasMaxLength(500);
            Property(p => p.Skype).HasMaxLength(100);

            HasMany(p => p.Users).WithRequired(u => u.Person).HasForeignKey(u => u.PersonId);
        }
    }
}
