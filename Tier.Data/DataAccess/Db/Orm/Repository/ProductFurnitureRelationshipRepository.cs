﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.DataAccess.Db.Orm.Repository
{
    public class ProductFurnitureRelationshipRepository : AbstractRepository<ProductFurnitureRelationship, MainDbContext>, IProductFurnitureRelationshipRepository
    {
        public ProductFurnitureRelationshipRepository() : base(new MainDbContext())
        {

        }

        public IEnumerable<ProductFurnitureRelationship> GetAll(Expression<Func<ProductFurnitureRelationship, bool>> predicate)
        {
            return Context.ProductFurnitureRelationships.Where(predicate).ToList();
        }

        public ProductFurnitureRelationship Get(Expression<Func<ProductFurnitureRelationship, bool>> predicate)
        {
            return Context.ProductFurnitureRelationships.Where(predicate).FirstOrDefault();
        }


        public void Delete(ProductFurnitureRelationship productRelationship)
        {
            Context.Entry(productRelationship).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public IEnumerable<ProductFurnitureRelationship> GetAll(short status)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProductFurnitureRelationship> GetAllByProductOwner(int productOwnerId, short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                GetAll(pt => pt.ProductOwnerId == productOwnerId && pt.Status != (short)EnumStatus.Deleted)
                :
                GetAll(pt => pt.ProductOwnerId == productOwnerId && pt.Status == status);
        }

        public void Save(ProductFurnitureRelationship productRelationship)
        {
            if (productRelationship.Id <= 0)
            {
                base.Add(productRelationship);
            }
            else
            {
                Context.Entry(productRelationship).State = EntityState.Modified;
                Context.SaveChanges();
            }
        }

        public ProductFurnitureRelationship GetByProductOwnerAndProductDependant(int productOwnerId, int productDependantId)
        {
            return Get(pr => pr.ProductOwnerId == productOwnerId && pr.ProductDependantId == productDependantId);
        }
    }
}
