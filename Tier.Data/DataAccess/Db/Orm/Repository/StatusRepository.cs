﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.DataAccess.Db.Orm.Repository
{
    public class StatusRepository : AbstractRepository<StatusEntity, MainDbContext>, IStatusRepository
    {
        public StatusRepository() : base(new MainDbContext())
        {

        }

        public IEnumerable<StatusEntity> GetAll(Expression<Func<StatusEntity, bool>> predicate)
        {
            return Context.Statuses.Where(predicate).ToList();
        }

        public IEnumerable<StatusEntity> GetAll(short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                GetAll(s => s.Status != (short)EnumStatus.Deleted)
                :
                GetAll(s => s.Status == status);
        }


        public StatusEntity Get(Expression<Func<StatusEntity, bool>> predicate)
        {
            return Context.Statuses.Where(predicate).FirstOrDefault();
        }

        public new StatusEntity Get(int statusId)
        {
            return Get(s => s.Id == statusId);
        }
    }
}
