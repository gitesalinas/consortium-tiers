﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.DataAccess.Db.Orm.Repository
{
    public class ResourceRepository : AbstractRepository<Resource, MainDbContext>, IResourceRepository
    {
        public ResourceRepository() : base(new MainDbContext())
        {

        }

        public Resource Get(Expression<Func<Resource, bool>> predicate)
        {
            return Context.Resources.Where(predicate).FirstOrDefault();
        }


        public void Save(Resource resource)
        {
            if(0 == resource.Id)
            {
                base.Add(resource);
            }
            else
            {
                Context.Entry(resource).State = EntityState.Modified;
                Context.SaveChanges();
            }
        }
    }
}
