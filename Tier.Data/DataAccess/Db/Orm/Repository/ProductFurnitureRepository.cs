﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.DataAccess.Db.Orm.Repository
{
    public class ProductFurnitureRepository : AbstractRepository<ProductFurniture, MainDbContext>, IProductFurnitureRepository
    {
        public ProductFurnitureRepository() : base(new MainDbContext())
        {

        }

        public IEnumerable<ProductFurniture> GetAllFull(short status)
        {
            throw new NotImplementedException();
        }

        public ProductFurniture GetFull(int id, short status)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<ProductFurniture> GetAll(Expression<Func<ProductFurniture, bool>> predicate)
        {
            return Context.ProductsFurniture.Where(predicate).ToList();
        }

        public IEnumerable<ProductFurniture> GetAll(short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                GetAll(p => p.Status != (short)EnumStatus.Deleted)
                :
                GetAll(p => p.Status == status);
        }

        public ProductFurniture Get(Expression<Func<ProductFurniture, bool>> predicate)
        {
            return Context.ProductsFurniture.Where(predicate).FirstOrDefault();
        }

        public ProductFurniture Get(int id, short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                Get(p => p.Status != (short)EnumStatus.Deleted && p.Id == id)
                :
                Get(p => p.Status == status && p.Id == id);
        }

        public void Save(ProductFurniture product)
        {
            if (0 == product.Id)
            {
                base.Add(product);
            }
            else
            {
                Context.Entry(product).State = EntityState.Modified;
                Context.SaveChanges();
            }
        }

        public void Delete(ProductFurniture product)
        {
            Context.Entry(product).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public IEnumerable<ProductFurniture> GetAllForRelationship(int productId, short status)
        {
            List<ProductFurniture> products = null;

            using (var connection = Context.Database.Connection as SqlConnection)
            {
                using (var command = new SqlCommand("spr_product_furniture_get_all_for_relationship", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@productId", productId);

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            products = new List<ProductFurniture>();

                            while (reader.Read())
                            {
                                products.Add(new ProductFurniture
                                {
                                    SKU = reader.GetColumnValue<string>("SKU"),
                                    Id = reader.GetColumnValue<int>("Id"),
                                    Name = reader.GetColumnValue<string>("Name"),
                                    Description = reader.GetColumnValue<string>("Description"),
                                    Tax = reader.GetColumnValue<decimal>("Tax"),
                                    UnitPrice = reader.GetColumnValue<decimal>("UnitPrice"),
                                    TotalPrice = reader.GetColumnValue<decimal>("TotalPrice"),
                                });
                            }
                        }
                    }

                    connection.Close();
                }
            }

            return products;

            //var x = Context.Database.SqlQuery<ProductFornitureTemp>("spr_product_get_all_for_relationship @productId", new object[] {
            //    new SqlParameter("@productId", productId)
            //}).ToList();

            //public class ProductFornitureTemp
            //{
            //    public string SKU { get; set; }
            //    public int Id { get; set; }
            //    public string Name { get; set; }
            //    public string Description { get; set; }
            //    public decimal Tax { get; set; }
            //    public decimal UnitPrice { get; set; }
            //    public decimal TotalPrice { get; set; }
            //}

            //return null;
        }
    }
}
