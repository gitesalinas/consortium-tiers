﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.DataAccess.Db.Orm.Repository
{
    public class RoomRepository : AbstractRepository<Room, MainDbContext>, IRoomRepository
    {
        public RoomRepository() : base(new MainDbContext())
        {
        }

        public IEnumerable<Room> GetAllFull(Expression<Func<Room, bool>> predicate)
        {
            return Context.Rooms.Include(r => r.RoomFacilities).Where(predicate).ToList();
        }

        public IEnumerable<Room> GetAllFull(short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                GetAllFull(r => r.Status != (short)EnumStatus.Deleted)
                :
                GetAllFull(r => r.Status == status);
        }

        public Room GetFull(Expression<Func<Room, bool>> predicate)
        {
            return Context.Rooms.Include(r => r.RoomFacilities).Where(predicate).FirstOrDefault();
        }

        public Room GetFull(int id, short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                GetFull(u => u.Status != (short)EnumStatus.Deleted && u.Id == id)
                :
                GetFull(u => u.Status == status && u.Id == id);
        }


        public IEnumerable<Room> GetAll(Expression<Func<Room, bool>> predicate)
        {
            return Context.Rooms.Where(predicate).ToList();
        }

        public IEnumerable<Room> GetAll(short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                GetAll(r => r.Status != (short)EnumStatus.Deleted)
                :
                GetAll(r => r.Status == status);
        }


        public Room Get(Expression<Func<Room, bool>> predicate)
        {
            return Context.Rooms.Where(predicate).FirstOrDefault();
        }

        public Room Get(int id, short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                Get(r => r.Status != (short)EnumStatus.Deleted && r.Id == id)
                :
                Get(r => r.Status == status && r.Id == id);
        }

        public void Save(Room Room)
        {
            if (Room.Id <= 0)
            {
                base.Add(Room);
            }
            else
            {
                Context.Entry(Room).State = EntityState.Modified;
                Context.SaveChanges();
            }
        }

        public void Delete(Room Room)
        {
            Context.Entry(Room).State = EntityState.Modified;
            Context.SaveChanges();
        }

        //public void AddRoomMedia(RoomMedia roomMedia)
        //{
        //    var resourceIdOuput = new SqlParameter() { Direction = ParameterDirection.Output, ParameterName = "@newResourceId", SqlDbType = SqlDbType.Int };
        //    var roomMediaIdOuput = new SqlParameter() { Direction = ParameterDirection.Output, ParameterName = "@newRoomMediaId", SqlDbType = SqlDbType.Int };

        //    Context.Database.ExecuteSqlCommand("spr_room_media_add @roomId, @isMain, @name, @extension, @contentLength, " +
        //        "@contentType, @publicName, @directoryPath, @path, @virtualDirectoryPath, @virtualPath, @thumbnailDirectoryPath, " +
        //        "@thumbnailPath, @thumbnailVirtualDirectoryPath, @thumbnailVirtualPath, @createdBy, @createdDate, @status, @newResourceId output, @newRoomMediaId output",
        //        new SqlParameter("@roomId", roomMedia.RoomId),
        //        new SqlParameter("@isMain", roomMedia.IsMain),
        //        new SqlParameter("@name", roomMedia.Resource.Name),
        //        new SqlParameter("@extension", roomMedia.Resource.Extension),
        //        new SqlParameter("@contentLength", roomMedia.Resource.ContentLength),
        //        new SqlParameter("@contentType", roomMedia.Resource.ContentType),
        //        new SqlParameter("@publicName", roomMedia.Resource.PublicName),
        //        new SqlParameter("@directoryPath", roomMedia.Resource.DirectoryPath),
        //        new SqlParameter("@path", roomMedia.Resource.Path),
        //        new SqlParameter("@virtualDirectoryPath", roomMedia.Resource.VirtualDirectoryPath),

        //        new SqlParameter("@virtualPath", roomMedia.Resource.VirtualPath),
        //        new SqlParameter("@thumbnailDirectoryPath", roomMedia.Resource.ThumbnailDirectoryPath),
        //        new SqlParameter("@thumbnailPath", roomMedia.Resource.ThumbnailPath),
        //        new SqlParameter("@thumbnailVirtualDirectoryPath", roomMedia.Resource.ThumbnailVirtualDirectoryPath),
        //        new SqlParameter("@thumbnailVirtualPath", roomMedia.Resource.ThumbnailVirtualPath),
        //        new SqlParameter("@createdBy", (roomMedia.Resource.Id <= 0 ? roomMedia.Resource.CreatedBy : roomMedia.Resource.UpdatedBy)),
        //        new SqlParameter("@createdDate", (roomMedia.Resource.Id <= 0 ? roomMedia.Resource.CreatedDate : roomMedia.Resource.UpdatedDate)),
        //        new SqlParameter("@status", roomMedia.Resource.Status),
        //        resourceIdOuput, roomMediaIdOuput);

        //    roomMedia.Id = (int)roomMediaIdOuput.Value;
        //    roomMedia.Resource.Id = (int)resourceIdOuput.Value;
        //}
    }
}
