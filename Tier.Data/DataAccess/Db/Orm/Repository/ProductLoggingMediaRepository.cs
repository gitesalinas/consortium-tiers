﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.DataAccess.Db.Orm.Repository
{
    public class ProductLoggingMediaRepository : AbstractRepository<ProductLoggingMedia, MainDbContext>, IProductLoggingMediaRepository
    {
        public ProductLoggingMediaRepository() : base(new MainDbContext())
        {

        }

        public void Delete(ProductLoggingMedia productMedia)
        {
            Context.Entry(productMedia).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public IEnumerable<ProductLoggingMedia> GetAllFull(Expression<Func<ProductLoggingMedia, bool>> predicate)
        {
            return Context.ProductLoggingMedias.Include(pm => pm.Product).Include(pm => pm.Resource).Where(predicate).ToList();
        }

        public ProductLoggingMedia GetFull(Expression<Func<ProductLoggingMedia, bool>> predicate)
        {
            return Context.ProductLoggingMedias.Include(pm => pm.Product).Include(pm => pm.Resource).Where(predicate).FirstOrDefault();
        }


        public ProductLoggingMedia Get(Expression<Func<ProductLoggingMedia, bool>> predicate)
        {
            return Context.ProductLoggingMedias.Where(predicate).FirstOrDefault();
        }

        public ProductLoggingMedia Get(int id, short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                    Get(pm => pm.Id == id && pm.Status != (short)EnumStatus.Deleted)
                    :
                    Get(pm => pm.Id == id && pm.Status == status);
        }

        public IEnumerable<ProductLoggingMedia> GetAll(Expression<Func<ProductLoggingMedia, bool>> predicate)
        {
            return Context.ProductLoggingMedias.Where(predicate).ToList();
        }

        public IEnumerable<ProductLoggingMedia> GetAll(short status)
        {
            return status.Equals(EnumStatus.None) ?
                    GetAll(rm => rm.Status != (short)EnumStatus.Deleted)
                    :
                    GetAll(rm => rm.Status == status);
        }

        public IEnumerable<ProductLoggingMedia> GetAllByProductId(int productId, short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                    GetAllFull(pm => pm.ProductId == productId && pm.Status != (short)EnumStatus.Deleted)
                    :
                    GetAllFull(pm => pm.ProductId == productId && pm.Status == status);
        }

        public void Save(ProductLoggingMedia productMedia)
        {
            if (productMedia.Id <= 0)
            {
                base.Add(productMedia);
            }
            else
            {
                Context.Entry(productMedia).State = EntityState.Modified;
                Context.SaveChanges();
            }
        }

        public void HandleMain(ProductLoggingMedia productMedia)
        {
            Context.Database.ExecuteSqlCommand("spr_product_logging_media_set_main @id, @productId, @isMain, @updatedBy, @updatedDate",
                new SqlParameter("@id", productMedia.Id),
                new SqlParameter("@productId", productMedia.ProductId),
                new SqlParameter("@isMain", productMedia.IsMain),
                new SqlParameter("@updatedBy", productMedia.UpdatedBy),
                new SqlParameter("@updatedDate", productMedia.UpdatedDate));
        }

        public ProductLoggingMedia GetMainProductMedia(int productId)
        {
            return Context.ProductLoggingMedias.Include(pm => pm.Resource).Where(pm => pm.IsMain == true && pm.ProductId == productId).FirstOrDefault();
        }

        public void AddProductMedia(ProductLoggingMedia productMedia)
        {
            var resourceIdOuput = new SqlParameter() { Direction = ParameterDirection.Output, ParameterName = "@newResourceId", SqlDbType = SqlDbType.Int };
            var productMediaIdOuput = new SqlParameter() { Direction = ParameterDirection.Output, ParameterName = "@newProductMediaId", SqlDbType = SqlDbType.Int };

            Context.Database.ExecuteSqlCommand("spr_product_logging_media_add @productId, @isMain, @name, @extension, @contentLength, " +
                "@contentType, @publicName, @directoryPath, @path, @virtualDirectoryPath, @virtualPath, @thumbnailDirectoryPath, " +
                "@thumbnailPath, @thumbnailVirtualDirectoryPath, @thumbnailVirtualPath, @createdBy, @createdDate, @status, @newResourceId output, @newProductMediaId output",
                new SqlParameter("@productId", productMedia.ProductId),
                new SqlParameter("@isMain", productMedia.IsMain),
                new SqlParameter("@name", productMedia.Resource.Name),
                new SqlParameter("@extension", productMedia.Resource.Extension),
                new SqlParameter("@contentLength", productMedia.Resource.ContentLength),
                new SqlParameter("@contentType", productMedia.Resource.ContentType),
                new SqlParameter("@publicName", productMedia.Resource.PublicName),
                new SqlParameter("@directoryPath", productMedia.Resource.DirectoryPath),
                new SqlParameter("@path", productMedia.Resource.Path),
                new SqlParameter("@virtualDirectoryPath", productMedia.Resource.VirtualDirectoryPath),

                new SqlParameter("@virtualPath", productMedia.Resource.VirtualPath),
                new SqlParameter("@thumbnailDirectoryPath", productMedia.Resource.ThumbnailDirectoryPath),
                new SqlParameter("@thumbnailPath", productMedia.Resource.ThumbnailPath),
                new SqlParameter("@thumbnailVirtualDirectoryPath", productMedia.Resource.ThumbnailVirtualDirectoryPath),
                new SqlParameter("@thumbnailVirtualPath", productMedia.Resource.ThumbnailVirtualPath),
                new SqlParameter("@createdBy", (productMedia.Resource.Id <= 0 ? productMedia.Resource.CreatedBy : productMedia.Resource.UpdatedBy)),
                new SqlParameter("@createdDate", (productMedia.Resource.Id <= 0 ? productMedia.Resource.CreatedDate : productMedia.Resource.UpdatedDate)),
                new SqlParameter("@status", productMedia.Resource.Status),
                resourceIdOuput, productMediaIdOuput);

            productMedia.Id = (int)productMediaIdOuput.Value;
            productMedia.Resource.Id = (int)resourceIdOuput.Value;
        }
    }
}
