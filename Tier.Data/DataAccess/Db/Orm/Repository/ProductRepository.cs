﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.DataAccess.Db.Orm.Repository
{
    public class ProductRepository : AbstractRepository<BaseProduct, MainDbContext>, IProductRepository
    {
        public ProductRepository() : base(new MainDbContext())
        {

        }

        //public IEnumerable<Product> GetAllFull(short status)
        //{
        //    throw new NotImplementedException();
        //}

        //public Product GetFull(int id, short status)
        //{
        //    throw new NotImplementedException();
        //}


        //public IEnumerable<Product> GetAll(Expression<Func<Product, bool>> predicate)
        //{
        //    return Context.Products.Where(predicate).ToList();
        //}

        //public IEnumerable<Product> GetAll(short status)
        //{
        //    return status.Equals((short)EnumStatus.None) ?
        //        GetAll(p => p.Status != (short)EnumStatus.Deleted)
        //        :
        //        GetAll(p => p.Status == status);
        //}

        //public Product Get(Expression<Func<Product, bool>> predicate)
        //{
        //    return Context.Products.Where(predicate).FirstOrDefault();
        //}

        //public Product Get(int id, short status)
        //{
        //    return status.Equals((short)EnumStatus.None) ?
        //        Get(p => p.Status != (short)EnumStatus.Deleted && p.Id == id)
        //        :
        //        Get(p => p.Status == status && p.Id == id);
        //}

        //public void Save(Product product)
        //{
        //    if (0 == product.Id)
        //    {
        //        base.Add(product);
        //    }
        //    else
        //    {
        //        Context.Entry(product).State = EntityState.Modified;
        //        Context.SaveChanges();
        //    }
        //}

        //public void Delete(Product product)
        //{
        //    Context.Entry(product).State = EntityState.Modified;
        //    Context.SaveChanges();
        //}

        public void Delete(BaseProduct product)
        {
            throw new NotImplementedException();
        }

        public BaseProduct Get(int id, short status = -1)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BaseProduct> GetAll(short status = -1)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BaseProduct> GetAllFull(short status = -1)
        {
            throw new NotImplementedException();
        }

        public BaseProduct GetFull(int id, short status = -1)
        {
            throw new NotImplementedException();
        }

        public void Save(BaseProduct product)
        {
            throw new NotImplementedException();
        }
    }
}
