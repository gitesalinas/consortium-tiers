﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.DataAccess.Db.Orm.Repository
{
    public class ProductTypeRepository : AbstractRepository<ProductType, MainDbContext>, IProductTypeRepository
    {
        public ProductTypeRepository() : base(new MainDbContext())
        {

        }

        public IEnumerable<ProductType> GetAll(Expression<Func<ProductType, bool>> predicate)
        {
            return Context.ProductTypes.Where(predicate).ToList();
        }

        public IEnumerable<ProductType> GetAll(short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                GetAll(pt => pt.Status != (short)EnumStatus.Deleted)
                :
                GetAll(pt => pt.Status == status);
        }


        public ProductType Get(Expression<Func<ProductType, bool>> predicate)
        {
            return Context.ProductTypes.Where(predicate).FirstOrDefault();
        }

        public new ProductType Get(int productType)
        {
            return Get(pt => pt.Id == productType);
        }

        public void Save(ProductType productType)
        {
            if (productType.Id <= 0)
            {
                base.Add(productType);
            }
            else
            {
                Context.Entry(productType).State = EntityState.Modified;
                Context.SaveChanges();
            }
        }

        public void Delete(ProductType productType)
        {
            Context.Entry(productType).State = EntityState.Modified;
            Context.SaveChanges();
        }
    }
}
