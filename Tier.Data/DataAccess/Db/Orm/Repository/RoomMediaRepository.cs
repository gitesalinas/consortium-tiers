﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.DataAccess.Db.Orm.Repository
{
    public class RoomMediaRepository : AbstractRepository<RoomMedia, MainDbContext>, IRoomMediaRepository
    {
        public RoomMediaRepository() : base(new MainDbContext())
        {

        }

        public void Delete(RoomMedia roomMedia)
        {
            Context.Entry(roomMedia).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public IEnumerable<RoomMedia> GetAllFull(Expression<Func<RoomMedia, bool>> predicate)
        {
            return Context.RoomsMedias.Include(rm => rm.Room).Include(rm => rm.Resource).Where(predicate).ToList();
        }

        public RoomMedia GetFull(Expression<Func<RoomMedia, bool>> predicate)
        {
            return Context.RoomsMedias.Include(rm => rm.Room).Include(rm => rm.Resource).Where(predicate).FirstOrDefault();
        }


        public RoomMedia Get(Expression<Func<RoomMedia, bool>> predicate)
        {
            return Context.RoomsMedias.Where(predicate).FirstOrDefault();
        }

        public RoomMedia Get(int id, short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                    Get(rm => rm.Id == id && rm.Status != (short)EnumStatus.Deleted)
                    :
                    Get(rm => rm.Id == id && rm.Status == status);
        }

        public IEnumerable<RoomMedia> GetAll(Expression<Func<RoomMedia, bool>> predicate)
        {
            return Context.RoomsMedias.Where(predicate).ToList();
        }

        public IEnumerable<RoomMedia> GetAll(short status)
        {
            return status.Equals(EnumStatus.None) ?
                    GetAll(rm => rm.Status != (short)EnumStatus.Deleted)
                    :
                    GetAll(rm => rm.Status == status);
        }

        public IEnumerable<RoomMedia> GetAllByRoomId(int roomId, short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                    GetAllFull(rm => rm.RoomId == roomId && rm.Status != (short)EnumStatus.Deleted)
                    :
                    GetAllFull(rm => rm.RoomId == roomId && rm.Status == status);
        }

        public void Save(RoomMedia roomMedia)
        {
            if (roomMedia.Id <= 0)
            {
                base.Add(roomMedia);
            }
            else
            {
                Context.Entry(roomMedia).State = EntityState.Modified;
                Context.SaveChanges();
            }
        }

        public void HandleMain(RoomMedia roomMedia)
        {
            Context.Database.ExecuteSqlCommand("spr_room_media_set_main @id, @roomId, @isMain, @updatedBy, @updatedDate",
                new SqlParameter("@id", roomMedia.Id),
                new SqlParameter("@roomId", roomMedia.RoomId),
                new SqlParameter("@isMain", roomMedia.IsMain),
                new SqlParameter("@updatedBy", roomMedia.UpdatedBy),
                new SqlParameter("@updatedDate", roomMedia.UpdatedDate));
        }

        public RoomMedia GetMainRoomMedia(int roomId)
        {
            return Context.RoomsMedias.Include(rm => rm.Resource).Where(rm => rm.IsMain == true && rm.RoomId == roomId).FirstOrDefault();
        }

        public void AddRoomMedia(RoomMedia roomMedia)
        {
            var resourceIdOuput = new SqlParameter() { Direction = ParameterDirection.Output, ParameterName = "@newResourceId", SqlDbType = SqlDbType.Int };
            var roomMediaIdOuput = new SqlParameter() { Direction = ParameterDirection.Output, ParameterName = "@newRoomMediaId", SqlDbType = SqlDbType.Int };

            Context.Database.ExecuteSqlCommand("spr_room_media_add @roomId, @isMain, @name, @extension, @contentLength, " +
                "@contentType, @publicName, @directoryPath, @path, @virtualDirectoryPath, @virtualPath, @thumbnailDirectoryPath, " +
                "@thumbnailPath, @thumbnailVirtualDirectoryPath, @thumbnailVirtualPath, @createdBy, @createdDate, @status, @newResourceId output, @newRoomMediaId output",
                new SqlParameter("@roomId", roomMedia.RoomId),
                new SqlParameter("@isMain", roomMedia.IsMain),
                new SqlParameter("@name", roomMedia.Resource.Name),
                new SqlParameter("@extension", roomMedia.Resource.Extension),
                new SqlParameter("@contentLength", roomMedia.Resource.ContentLength),
                new SqlParameter("@contentType", roomMedia.Resource.ContentType),
                new SqlParameter("@publicName", roomMedia.Resource.PublicName),
                new SqlParameter("@directoryPath", roomMedia.Resource.DirectoryPath),
                new SqlParameter("@path", roomMedia.Resource.Path),
                new SqlParameter("@virtualDirectoryPath", roomMedia.Resource.VirtualDirectoryPath),

                new SqlParameter("@virtualPath", roomMedia.Resource.VirtualPath),
                new SqlParameter("@thumbnailDirectoryPath", roomMedia.Resource.ThumbnailDirectoryPath),
                new SqlParameter("@thumbnailPath", roomMedia.Resource.ThumbnailPath),
                new SqlParameter("@thumbnailVirtualDirectoryPath", roomMedia.Resource.ThumbnailVirtualDirectoryPath),
                new SqlParameter("@thumbnailVirtualPath", roomMedia.Resource.ThumbnailVirtualPath),
                new SqlParameter("@createdBy", (roomMedia.Resource.Id <= 0 ? roomMedia.Resource.CreatedBy : roomMedia.Resource.UpdatedBy)),
                new SqlParameter("@createdDate", (roomMedia.Resource.Id <= 0 ? roomMedia.Resource.CreatedDate : roomMedia.Resource.UpdatedDate)),
                new SqlParameter("@status", roomMedia.Resource.Status),
                resourceIdOuput, roomMediaIdOuput);

            roomMedia.Id = (int)roomMediaIdOuput.Value;
            roomMedia.Resource.Id = (int)resourceIdOuput.Value;
        }
    }
}
