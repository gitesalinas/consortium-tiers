﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.DataAccess.Db.Orm.Repository
{
    public class FacilityRepository : AbstractRepository<Facility, MainDbContext>, IFacilityRepository
    {
        public FacilityRepository() : base(new MainDbContext())
        {
        }

        public IEnumerable<Facility> GetAll(Expression<Func<Facility, bool>> predicate)
        {
            return Context.Facilities.Where(predicate).ToList();
        }

        public IEnumerable<Facility> GetAll(short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                GetAll(f => f.Status != (short)EnumStatus.Deleted)
                :
                GetAll(f => f.Status == status);
        }

        public Facility Get(Expression<Func<Facility, bool>> predicate)
        {
            return Context.Facilities.Where(predicate).FirstOrDefault();
        }

        public Facility Get(int id, short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                Get(f => f.Status != (short)EnumStatus.Deleted && f.Id == id)
                :
                Get(f => f.Status == status && f.Id == id);
        }

        public void Save(Facility facility)
        {
            if (facility.Id <= 0)
            {
                base.Add(facility);
            }
            else
            {
                Context.Entry(facility).State = EntityState.Modified;
                Context.SaveChanges();
            }
        }

        public void Delete(Facility facility)
        {
            Context.Entry(facility).State = EntityState.Modified;
            Context.SaveChanges();
        }
    }
}
