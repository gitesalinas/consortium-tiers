﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.DataAccess.Db.Orm.Repository
{
    public class UserTypeRepository : AbstractRepository<UserType, MainDbContext>, IUserTypeRepository
    {
        public UserTypeRepository() : base(new MainDbContext())
        {

        }

        public IEnumerable<UserType> GetAll(Expression<Func<UserType, bool>> predicate)
        {
            return Context.UserTypes.Where(predicate).ToList();
        }

        public IEnumerable<UserType> GetAll(short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                GetAll(ut => ut.Status != (short)EnumStatus.Deleted)
                :
                GetAll(ut => ut.Status == status);
        }


        public UserType Get(Expression<Func<UserType, bool>> predicate)
        {
            return Context.UserTypes.Where(predicate).FirstOrDefault();
        }

        public new UserType Get(int userTypeId)
        {
            return Get(ut => ut.Id == userTypeId);
        }

        public void Save(UserType userType)
        {
            if (userType.Id <= 0)
            {
                base.Add(userType);
            }
            else
            {
                Context.Entry(userType).State = EntityState.Modified;
                Context.SaveChanges();
            }
        }

        public void Delete(UserType userType)
        {
            Context.Entry(userType).State = EntityState.Modified;
            Context.SaveChanges();
        }
    }
}
