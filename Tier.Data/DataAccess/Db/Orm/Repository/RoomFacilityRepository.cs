﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.DataAccess.Db.Orm.Repository
{
    public class RoomFacilityRepository : AbstractRepository<RoomFacility, MainDbContext>, IRoomFacilityRepository
    {
        public RoomFacilityRepository() : base(new MainDbContext())
        {
        }

        public void Delete(RoomFacility roomFacility)
        {
            Context.Entry(roomFacility).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public RoomFacility Get(Expression<Func<RoomFacility, bool>> predicate)
        {
            return Context.RoomsFacilities.Where(predicate).FirstOrDefault();
        }

        public RoomFacility Get(int id, short status)
        {
            return status.Equals(EnumStatus.None) ?
                    Get(rf => rf.Id == id && rf.Status != (short)EnumStatus.Deleted)
                    :
                    Get(rf => rf.Id == id && rf.Status == status);
        }

        public IEnumerable<RoomFacility> GetAll(Expression<Func<RoomFacility, bool>> predicate)
        {
            return Context.RoomsFacilities.Where(predicate).ToList();
        }

        public IEnumerable<RoomFacility> GetAll(short status)
        {
            return status.Equals(EnumStatus.None) ?
                    GetAll(rf => rf.Status != (short)EnumStatus.Deleted)
                    :
                    GetAll(rf => rf.Status == status);
        }

        public IEnumerable<RoomFacility> GetAllByRoomId(int roomId, short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                    GetAll(rf => rf.RoomId == roomId && rf.Status != (short)EnumStatus.Deleted)
                    :
                    GetAll(rf => rf.RoomId == roomId && rf.Status == status);
        }

        public RoomFacility GetAny(int id)
        {
            return Context.RoomsFacilities.Where(rf => rf.Id == id).FirstOrDefault();
        }

        public RoomFacility GetAnyByRoomIdAndFacilityId(int roomId, int facilityId)
        {
            return Context.RoomsFacilities.Where(rf => rf.RoomId == roomId && rf.FacilityId == facilityId).FirstOrDefault();
        }

        public void Save(RoomFacility roomFacility)
        {
            if (roomFacility.Id <= 0)
            {
                base.Add(roomFacility);
            }
            else
            {
                Context.Entry(roomFacility).State = EntityState.Modified;
                Context.SaveChanges();
            }
        }
    }
}
