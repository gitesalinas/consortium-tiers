﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.DataAccess.Db.Orm.Repository
{
    public class UserRepository : AbstractRepository<User, MainDbContext>, IUserRepository
    {
        public UserRepository() : base(new MainDbContext())
        {
            
        }


        public IEnumerable<User> GetAllFull(Expression<Func<User, bool>> predicate)
        {
            return Context.Users
                .Include(u => u.Type)
                .Include(u => u.Picture)
                .Include(u => u.Person).Where(predicate).ToList();
        }

        public IEnumerable<User> GetAllFull(short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                GetAllFull(u => u.Status != (short)EnumStatus.Deleted)
                :
                GetAllFull(u => u.Status == status);
        }

        public User GetFull(Expression<Func<User, bool>> predicate)
        {
            return Context.Users
                .Include(u => u.Type)
                .Include(u => u.Person)
                .Include(u => u.Picture).Where(predicate).FirstOrDefault();
        }

        public User GetFull(int id, short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                GetFull(u => u.Status != (short)EnumStatus.Deleted && u.Id == id)
                :
                GetFull(u => u.Status == status && u.Id == id);
        }

        


        public IEnumerable<User> GetAll(Expression<Func<User, bool>> predicate)
        {
            return Context.Users.Where(predicate).ToList();
        }

        public IEnumerable<User> GetAll(short status)
        {
            return status.Equals((short)EnumStatus.None) ? 
                GetAll(u => u.Status != (short)EnumStatus.Deleted)
                :
                GetAll(u => u.Status == status);
        }


        public User Get(Expression<Func<User, bool>> predicate)
        {
            return Context.Users.Where(predicate).FirstOrDefault();
        }

        public User Get(int id, short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                Get(u => u.Status != (short)EnumStatus.Deleted && u.Id == id)
                :
                Get(u => u.Status == status && u.Id == id);
        }




        public User GetProfile(int id)
        {
            return GetFull(u => u.Id == id);
        }

        public User GetWithPerson(int id)
        {
            return Context.Users.Include(u => u.Person)
                .Where(u => u.Id == id).FirstOrDefault();
        }



        public User Login(string username, string password)
        {
            return GetFull(u => u.Username == username && u.Password == password);
        }

        public void Register(User user)
        {
            base.Add(user);
            //Context.Users.Add(user);
            //Context.SaveChanges();
        }

        public void SaveProfile(User user)
        {
            /*object[] @params = {
                new SqlParameter("@userId", user.Id),
                new SqlParameter("@personId", user.Person.Id),

                new SqlParameter("@firstname", user.Person.Firstname),
                new SqlParameter("@lastname", user.Person.Lastname),
                new SqlParameter("@email", user.Person.Email),
                new SqlParameter("@gender", user.Person.Gender),
                new SqlParameter("@birth", user.Person.Birth),
                new SqlParameter("@homePhone", user.Person.HomePhone ?? SqlString.Null),
                new SqlParameter("@workPhone", user.Person.WorkPhone ?? SqlString.Null),
                new SqlParameter("@mobilPhone", user.Person.MobilPhone ?? SqlString.Null),

                new SqlParameter("@website", user.Person.Website ?? SqlString.Null),
                new SqlParameter("@facebook", user.Person.Facebook ?? SqlString.Null),
                new SqlParameter("@twitter", user.Person.Twitter ?? SqlString.Null),
                new SqlParameter("@googlePlus", user.Person.GooglePlus ?? SqlString.Null),
                new SqlParameter("@linkedin", user.Person.Linkedin ?? SqlString.Null),
                new SqlParameter("@skype", user.Person.Skype ?? SqlString.Null)
            };

            Context.Database.ExecuteSqlCommand("spr_save_profile @userId, @personId, @firstname, @lastname, @email, @gender, @birth, @homePhone, @workPhone, @mobilPhone, @website, @facebook, @twitter, @googlePlus, @linkedin, @skype",
                @params);*/

            Context.Entry(user).State = EntityState.Modified;
            Context.Entry(user.Person).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public void SaveProfilePicture(User user, Resource resource)
        {
            var output = new SqlParameter() { Direction = ParameterDirection.Output, ParameterName = "@newId", SqlDbType = SqlDbType.Int };

            Context.Database.ExecuteSqlCommand("spr_profile_picture_save @userId, @resourceId, @name, @extension, @contentLength, " +
                "@contentType, @publicName, @directoryPath, @path, @virtualDirectoryPath, @virtualPath, @thumbnailDirectoryPath, " +
                "@thumbnailPath, @thumbnailVirtualDirectoryPath, @thumbnailVirtualPath, @createdBy, @createdDate, @status, @newId output",
                new SqlParameter("@userId", user.Id),
                new SqlParameter("@resourceId", resource.Id),
                new SqlParameter("@name", resource.Name),
                new SqlParameter("@extension", resource.Extension),
                new SqlParameter("@contentLength", resource.ContentLength),
                new SqlParameter("@contentType", resource.ContentType),
                new SqlParameter("@publicName", resource.PublicName),
                new SqlParameter("@directoryPath", resource.DirectoryPath),
                new SqlParameter("@path", resource.Path),
                new SqlParameter("@virtualDirectoryPath", resource.VirtualDirectoryPath),

                new SqlParameter("@virtualPath", resource.VirtualPath),
                new SqlParameter("@thumbnailDirectoryPath", resource.ThumbnailDirectoryPath),
                new SqlParameter("@thumbnailPath", resource.ThumbnailPath),
                new SqlParameter("@thumbnailVirtualDirectoryPath", resource.ThumbnailVirtualDirectoryPath),
                new SqlParameter("@thumbnailVirtualPath", resource.ThumbnailVirtualPath),
                new SqlParameter("@createdBy", (resource.Id <= 0 ? resource.CreatedBy : resource.UpdatedBy)),
                new SqlParameter("@createdDate", (resource.Id <= 0 ? resource.CreatedDate : resource.UpdatedDate)),
                new SqlParameter("@status", resource.Status),
                output);

            resource.Id = (int)output.Value;
        }

        public void Save(User user)
        {
            if(user.Id <= 0)
            {
                Context.Users.Add(user);
            }
            else
            {
                Context.Entry(user).State = EntityState.Modified;
                Context.Entry(user.Person).State = EntityState.Modified;
                Context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            UpdateStatus(id, (short)EnumStatus.Deleted);
        }

        public void UpdateStatus(int id, short status)
        {
            var userId = new SqlParameter("@id", id);
            var statusId = new SqlParameter("@status", status);

            Context.Database.ExecuteSqlCommand("spr_user_status @id, @status", userId, statusId);
        }
    }
}
