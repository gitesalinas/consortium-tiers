﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Data.DataAccess.Db.Orm.Repository
{
    public class ProductLoggingRepository : AbstractRepository<ProductLogging, MainDbContext>, IProductLoggingRepository
    {
        public ProductLoggingRepository() : base(new MainDbContext())
        {

        }

        public IEnumerable<ProductLogging> GetAllFull(short status)
        {
            throw new NotImplementedException();
        }

        public ProductLogging GetFull(int id, short status)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<ProductLogging> GetAll(Expression<Func<ProductLogging, bool>> predicate)
        {
            return Context.ProductsLogging.Where(predicate).ToList();
        }

        public IEnumerable<ProductLogging> GetAll(short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                GetAll(p => p.Status != (short)EnumStatus.Deleted)
                :
                GetAll(p => p.Status == status);
        }

        public ProductLogging Get(Expression<Func<ProductLogging, bool>> predicate)
        {
            return Context.ProductsLogging.Where(predicate).FirstOrDefault();
        }

        public ProductLogging Get(int id, short status)
        {
            return status.Equals((short)EnumStatus.None) ?
                Get(p => p.Status != (short)EnumStatus.Deleted && p.Id == id)
                :
                Get(p => p.Status == status && p.Id == id);
        }

        public void Save(ProductLogging product)
        {
            if (0 == product.Id)
            {
                base.Add(product);
            }
            else
            {
                Context.Entry(product).State = EntityState.Modified;
                Context.SaveChanges();
            }
        }

        public void Delete(ProductLogging product)
        {
            Context.Entry(product).State = EntityState.Modified;
            Context.SaveChanges();
        }
    }
}
