﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Http.Contract
{
    public interface IUserRepository : Tier.Data.Contract.IUserRepository, IRepository<User>
    {

    }
}
