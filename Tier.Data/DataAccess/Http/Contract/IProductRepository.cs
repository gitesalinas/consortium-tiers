﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Entity;

namespace Tier.Data.DataAccess.Http.Contract
{
    public interface IProductRepository : Tier.Data.Contract.IProductRepository, IRepository<BaseProduct>
    {

    }
}
