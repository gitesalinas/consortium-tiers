﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;

namespace Tier.Data.DataAccess.Http.Repository
{
    public class ProductRepository : AbstractRepository<BaseProduct>, IProductRepository
    {
        public ProductRepository()
        {
        }

        public void Delete(BaseProduct product)
        {
            throw new NotImplementedException();
        }

        public BaseProduct Get(int id, short status = -1)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BaseProduct> GetAll(short status = -1)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BaseProduct> GetAllFull(short status = -1)
        {
            throw new NotImplementedException();
        }

        public BaseProduct GetFull(int id, short status = -1)
        {
            throw new NotImplementedException();
        }

        public void Save(BaseProduct product)
        {
            throw new NotImplementedException();
        }
    }
}
