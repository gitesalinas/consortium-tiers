﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Entity;
using static Tier.Data.DataAccess.Http.MainHttp;

namespace Tier.Data.DataAccess.Http.Repository
{
    public class UserRepository : AbstractRepository<User>, IUserRepository
    {
        public UserRepository()
        {
            
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public User Get(int id, short status)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetAll(short status)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetAllFull(short status = -1)
        {
            throw new NotImplementedException();
        }

        public User GetFull(int id, short status = -1)
        {
            throw new NotImplementedException();
        }

        public User GetProfile(int userId)
        {
            throw new NotImplementedException();
        }

        public User GetWithPerson(int userId)
        {
            throw new NotImplementedException();
        }

        public User Login(string username, string password)
        {
            MainHttpBuilder builder = new MainHttpBuilder();

            builder
                .AddExtraUrl("UserAPI/API/Authenticate/")
                //.AcceptJSON(true)
                .AddHeader(new Dictionary<string, string>() {
                    { "eder", "salinas" },
                    //{ "Accept", "application/json" }
                })
                .AddAuthenticate(new Authenticate() { Username = username, Password = password });

            MainHttp mainHttp = builder.Build();

            return mainHttp.Get<User>();
        }

        public void Register(User user)
        {
            throw new NotImplementedException();
        }

        public void Save(User user)
        {
            throw new NotImplementedException();
        }

        public void SaveProfile(User user)
        {
            throw new NotImplementedException();
        }

        public void SaveProfilePicture(User user, Resource resource)
        {
            throw new NotImplementedException();
        }

        public void UpdateStatus(int id, short status = -1)
        {
            throw new NotImplementedException();
        }
    }
}
