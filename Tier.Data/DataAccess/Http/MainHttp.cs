﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Data.DataAccess.Http
{
    public class MainHttp
    {
        private static readonly string _baseUrl = "http://localhost:9757/";

        private IServiceClient Client { get; set; }

        public MainHttp(IServiceClient client)
        {
            this.Client = client;
        }

        public T Get<T>(string extraUrl = "")
        {
            return Client.Get<T>(extraUrl);
        }

        public class MainHttpBuilder
        {
            //private readonly string ACCEPT_JSON = "application/json";
            //private readonly string ACCEPT_XML = "application/xml";

            private String BaseUrl { get; set; }
            private String ExtraUrl { get; set; }
            private Authenticate Authenticate { get; set; }
            private Dictionary<string, string> Header { get; set; }
            //public Boolean ShouldAcceptJSON { get; set; }
            //public Boolean ShouldAcceptXML { get; set; }

            public MainHttpBuilder()
            {

            }

            public MainHttpBuilder AddBaseUrl(string baseUrl)
            {
                BaseUrl = baseUrl;
                return this;
            }

            public MainHttpBuilder AddExtraUrl(string extraUrl)
            {
                ExtraUrl = extraUrl;
                return this;
            }

            public MainHttpBuilder AddAuthenticate(Authenticate authenticate)
            {
                Authenticate = authenticate;
                return this;
            }

            public MainHttpBuilder AddHeader(Dictionary<string, string> header)
            {
                Header = header;
                return this;
            }

            //public MainHttpBuilder AcceptJSON(bool shouldAcceptJSON)
            //{
                //ShouldAcceptJSON = shouldAcceptJSON;
                //return this;
            //}

            //public MainHttpBuilder AcceptXML(bool shouldAcceptXML)
            //{
                //ShouldAcceptXML = shouldAcceptXML;
                //return this;
            //}

            public MainHttp Build()
            {
                if (String.IsNullOrEmpty(BaseUrl)) BaseUrl = MainHttp._baseUrl;

                IServiceClient client = new JsonServiceClient((BaseUrl + ExtraUrl));

                if(null != Authenticate 
                    && !String.IsNullOrEmpty(Authenticate.Username) 
                    && !String.IsNullOrEmpty(Authenticate.Password))
                {
                    ((JsonServiceClient)client).AlwaysSendBasicAuthHeader = true;
                    ((JsonServiceClient)client).UserName = Authenticate.Username;
                    ((JsonServiceClient)client).Password = Authenticate.Password;
                }

                if (null != Header && 0 < Header.Keys.Count)
                {
                    foreach (var item in Header)
                    {
                        client.AddHeader(item.Key, item.Value);
                    }
                }

                //if (ShouldAcceptXML) { client.AddHeader("Accept", ACCEPT_XML); }
                //if (ShouldAcceptJSON) { client.AddHeader("Accept", ACCEPT_JSON); }

                return new MainHttp(client);
            }
        }

        public class Authenticate
        {
            public Authenticate()
            {

            }

            public String Username { get; set; }
            public String Password { get; set; }
        }
    }
}
