﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Data.Factory.AbstractFactory;

namespace Tier.Data.Factory.ConcreteFactory
{
    public class ConcreteGenericFactory : AbstractGenericFactory
    {
        public ConcreteGenericFactory()
        {

        }

        //public override T UserRepository<T>(EnumRepository @enum)
        //{
        //    switch (@enum)
        //    {
        //        case EnumRepository.Http: return new T();
        //        case EnumRepository.Orm: return new T();
        //        default: throw new Exception("Repository Not Found");
        //    }
        //}

        //public override T ProductRepository<T>(EnumRepository @enum)
        //{
        //    switch (@enum)
        //    {
        //        case EnumRepository.Http: return new T();
        //        case EnumRepository.Orm: return new T();
        //        default: throw new Exception("Repository Not Found");
        //    }
        //}

        //public override T UserRepository<T>()
        //{
        //    return new T();
        //}

        //public override T ProductRepository<T>()
        //{
        //    return new T();
        //}

        public override T CreateObject<T>()
        {
            return new T();
        }
    }
}
