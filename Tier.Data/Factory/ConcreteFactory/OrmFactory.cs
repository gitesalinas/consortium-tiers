﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Data.DataAccess.Db.Orm.Repository;
using Tier.Data.Factory.AbstractFactory;

namespace Tier.Data.Factory.ConcreteFactory
{
    public class OrmFactory : IAbstractFactory
    {
        public IProductRepository ProductRepository()
        {
            return new ProductRepository();
        }

        public IUserRepository UserRepository()
        {
            return new UserRepository();
        }
    }
}
