﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;

namespace Tier.Data.Factory.AbstractFactory
{
    public interface IAbstractFactory
    {
        IUserRepository UserRepository();
        //IPersonRepository PersonRepository();
        IProductRepository ProductRepository();
    }
}
