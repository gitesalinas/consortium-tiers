﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;

namespace Tier.Data.Factory.AbstractFactory
{
    public abstract class AbstractGenericFactory
    {
        //public enum EnumRepository
        //{
        //    Database,
        //    Http,
        //    Orm
        //}

        //public abstract T UserRepository<T>(EnumRepository @enum) where T : new();
        //public abstract T ProductRepository<T>(EnumRepository @enum) where T : new();

        //public abstract T UserRepository<T>() where T : new();
        //public abstract T ProductRepository<T>() where T : new();

        public abstract T CreateObject<T>() where T : new();
    }
}
