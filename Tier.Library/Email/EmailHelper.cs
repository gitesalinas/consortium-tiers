﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Tier.Library.Secure;

namespace Tier.Library.Email
{
    public class EmailHelper
    {
        public class clsEmailServer
        {

            public String EmailName { get; set; }
            public String EmailSender { get; set; }
            public String SMTP { get; set; }
            public String Port { get; set; }
            public String Username { get; set; }
            public String Password { get; set; }
            public Boolean EnableSSL { get; set; }
        }

        public class MailView
        {
            public MailView()
            {

            }
            public MailView(String message, String subject, String mailFrom)
            {
                //this.mailTo = mailTo;
                this.message = message;
                this.subject = subject;
                this.mailFrom = mailFrom;
            }
            public List<String> mailTo { get; set; }
            public String message { get; set; }
            public String subject { get; set; }
            public String mailFrom { get; set; }
        }

        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            MailView token = (MailView)e.UserState;
            //BaseEntity ent = new BaseEntity();
            //Boolean isCancelled = false;

            if (e.Cancelled)
            {
                //isCancelled = true;
            }
            if (e.Error != null)
            {
                //Debug.WriteLine(token.subject + "  " + e.Error.ToString() + "  Error");
                for (int i = 0; i < token.mailTo.Count; i++)
                {
                    //clsSendEmail.SendEmailAsync("andres_13_83@hotmail.com"
                    //                , "holaaaaaaa"
                    //                , "hola mundo", "noreply@aseamail.com");
                    //SendEmailSupport(token.mailTo[i], token.subject, token.message);

                    //LogEmail_Insert(ref ent, token.mailTo[i], token.subject, token.message, token.mailFrom, e.Error.ToString(), isCancelled ? Convert.ToInt16(EnumEmailStatus.Cancelled) : Convert.ToInt16(EnumEmailStatus.Error));
                    //  clsUtilities.RegisterFailDomain("", "", "LogEMAIL - 1", "ERROR - 1");
                }
            }
            else
            {
                //Debug.WriteLine(token.subject + "Enviado!!!!!!!!!!!!!!!!!");
                //for (int i = 0; i < token.mailTo.Count; i++)
                //{

                    //LogEmail_Insert(ref ent, token.mailTo[i], token.subject, token.message, token.mailFrom, "", Convert.ToInt16(EnumEmailStatus.Success));
                    //clsUtilities.RegisterFailDomain("", "", "LogEMAIL - 2", "ERROR - 1");
                //}
            }
        }

        public static bool SendEmailAsync(List<String> mailTo, string message, string subject)
        {
            try
            {
                //If LoadConfiguration("EMAIL_DISABLED") = "1" Then Exit Function

                //string mailFrom = "test@xsscenter.com";
                ////""
                //string mailServer = "smtp.fatcow.com";
                ////""
                //int mailPort = 587;
                ////Dim mailSSL As Boolean = gObjConfiguracion.MailSSL
                //string mailFromAccount = "test@xsscenter.com";
                ////"activosfijos@grupoxentry.com"
                //string mailFromPassword = "@Xirect2012";
                ////"13011970"
                clsEmailServer server = null;//GetEmailServer(ref Base);

                if (server != null)
                {
                    //NUEVAS CREDENCIALES AQUI!!!
                    //NO OLVIDAR PONER DISPLAY NAME "Replicated Sites Test"

                    string mailFrom = server.EmailSender.ToString();
                    string mailServer = server.SMTP.ToString();
                    int mailPort = Convert.ToInt32(server.Port);
                    string mailFromAccount = server.Username.ToString();

                    //string mailFromAccount = "lpacheco@xirectss.com";
                    //"activosfijos@grupoxentry.com"
                    //string mailFromPassword = "3vtgbn7u";//   clsEncryption.Decrypt(obj.Password);

                    string mailFromPassword = EncryptionHelper.Decrypt(server.Password.ToString());
                    string mailSubject = subject;
                    string mailBody = message;
                    //Dim MediaType As String = "text/html"
                    mailBody = message;
                    System.Net.Mail.MailMessage insMail = new System.Net.Mail.MailMessage();
                    for (int i = 0; i < mailTo.Count; i++)
                    {
                        insMail.To.Add(mailTo[i]);
                    }

                    //Dim HTMLContent As System.Net.Mail.AlternateView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(mailBody, Nothing, MediaType)
                    //insMail.AlternateViews.Add(HTMLContent)

                    var _with1 = insMail;
                    _with1.Subject = mailSubject;
                    _with1.Body = mailBody;
                    _with1.IsBodyHtml = true;
                    //envia el mensaje como html
                    _with1.From = new System.Net.Mail.MailAddress(mailFrom, "Wealth Generators Notification");
                    _with1.ReplyTo = new System.Net.Mail.MailAddress(mailFrom);
                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                    smtp.Host = mailServer;
                    smtp.Port = mailPort;
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new System.Net.NetworkCredential(mailFromAccount, mailFromPassword);

                    //SendCompletedEventHandler a = new SendCompletedEventHandler();

                    smtp.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
                    MailView obj = new MailView(message, subject, mailFrom);
                    obj.mailTo = new List<string>();
                    obj.mailTo = mailTo;
                    //string userState = "Message 1";
                    smtp.SendAsync(insMail, obj);
                    //smtp.Send(insMail);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        /// <summary>
        /// Envio de Correo sincrono
        /// </summary>
        /// <param name="mailTo"></param>
        /// <param name="message"></param>
        /// <param name="subject"></param>
        /// <returns></returns>
        public static bool SendEmail(string mailTo, string message, string subject)
        {

            try
            {
                //If LoadConfiguration("EMAIL_DISABLED") = "1" Then Exit Function
                clsEmailServer server = null;//GetEmailServer(ref Base);

                if (server != null)
                {
                    //string mailFrom = "test@xsscenter.com";
                    string mailFrom = server.EmailSender.ToString();
                    //""
                    //string mailServer = "smtp.fatcow.com";
                    string mailServer = server.SMTP.ToString();
                    //""
                    //int mailPort = 587;
                    int mailPort = Convert.ToInt32(server.Port);
                    //Dim mailSSL As Boolean = gObjConfiguracion.MailSSL
                    //string mailFromAccount = "test@xsscenter.com";
                    string mailFromAccount = server.Username.ToString();
                    //"activosfijos@grupoxentry.com"
                    //string mailFromPassword = "@Xirect2012";
                    string mailFromPassword = EncryptionHelper.Decrypt(server.Password.ToString());
                    //"13011970"

                    string mailSubject = subject;
                    string mailBody = message;
                    //Dim MediaType As String = "text/html"
                    mailBody = message;
                    System.Net.Mail.MailMessage insMail = new System.Net.Mail.MailMessage(new System.Net.Mail.MailAddress(mailFrom), new System.Net.Mail.MailAddress(mailTo));

                    //Dim HTMLContent As System.Net.Mail.AlternateView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(mailBody, Nothing, MediaType)
                    //insMail.AlternateViews.Add(HTMLContent)

                    var _with1 = insMail;
                    _with1.Subject = mailSubject;
                    _with1.Body = mailBody;
                    _with1.IsBodyHtml = true;
                    //envia el mensaje como html
                    _with1.From = new System.Net.Mail.MailAddress(mailFrom);
                    _with1.ReplyTo = new System.Net.Mail.MailAddress(mailFrom);
                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                    smtp.Host = mailServer;
                    smtp.Port = mailPort;
                    smtp.EnableSsl = mailPort == 25 ? true : false;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new System.Net.NetworkCredential(mailFromAccount, mailFromPassword);

                    smtp.Send(insMail);

                    return true;
                }
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
