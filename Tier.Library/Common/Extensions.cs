﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Tier.Library.Common
{
    public static class Extensions
    {
        public static bool HasFile(this HttpPostedFileBase file)
        {
            return (null != file && file.ContentLength > 0);
        }


        #region DataExtensions

        //Those static methods extends SqlDataReader in order to avoid DBNull exceptions on casting types
        //on Data Access Layer:

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <typeparam name="T">The type of the data stored in the record.</typeparam>
        /// <param name="record">The record.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
        public static T GetColumnValue<T>(this SqlDataReader record, string columnName)
        {
            return GetColumnValue<T>(record, columnName, default(T));
        }


        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <typeparam name="T">The type of the data stored in the record</typeparam>
        /// <param name="record">The record.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="defaultValue">The value to return if the column contains a <value>DBNull.Value</value> value.</param>
        /// <returns></returns>
        public static T GetColumnValue<T>(this SqlDataReader reader, String columnName, T defaultValue)
        {
            try
            {
                object value = reader[columnName];
                if (value == null || value == DBNull.Value)
                {
                    return defaultValue;
                }
                else
                {
                    if (defaultValue == null && reader[columnName] is DateTime)
                    {
                        DateTime date = (DateTime)value;
                        object sdate = date.ToString("MM/dd/yyyy");
                        return (T)sdate;
                    }
                    else

                        return (T)value;
                }
            }
            catch (Exception)
            {

                return defaultValue;
            }

        }

        #endregion  



        //public static TValue NullSafe<T, TValue>(this T obj, Func<T, TValue> value)
        //{
        //    try
        //    {
        //        return value(obj);
        //    }
        //    catch (Exception/*Exception can be better choice instead of NullReferenceException*/)
        //    {
        //        return default(TValue);
        //    }
        //}
    }
}
