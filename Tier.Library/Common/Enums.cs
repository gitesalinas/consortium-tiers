﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static Tier.Library.Common.Enums;

namespace Tier.Library.Common
{
    public static class Enums
    {
        #region Extensions

        public static string GetStringValue(this Enum @enum, int index = 0)
        {
            string stringValue = null;

            Type type = @enum.GetType();

            FieldInfo field = type.GetField(@enum.ToString());

            StringValueAttribute[] attributes = field.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];

            if (null != attributes && 0 < attributes.Length)
                stringValue = attributes[index].StringValue;

            return stringValue;
        }

        #endregion

        #region Attributes

        public class StringValueAttribute : Attribute
        {
            public string StringValue { get; private set; }

            public StringValueAttribute(string stringValue)
            {
                StringValue = stringValue;
            }
        }

        #endregion
    }

    public enum EnumStatus
    {
        [StringValue("None")]
        None = -1,
        [StringValue("Disabled")]
        Disabled = 0,
        [StringValue("Enabled")]
        Enabled = 1,
        [StringValue("Deleted")]
        Deleted = 2
    }

    public enum EnumResponseErrorCode
    {
        General = 555,
        SessionTimeOut = 556
    }

    public enum EnumUserType
    {
        [StringValue("User")]
        User = 0,
        [StringValue("Admin")]
        Admin = 99,
        [StringValue("SuperUser")]
        SuperUser = 100
    }
}
