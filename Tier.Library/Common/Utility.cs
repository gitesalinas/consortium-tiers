﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Library.Common
{
    public class Utility
    {
        public static string GeneratePublicNames(string prefix = "", string suffix = "")
        {
            string publicName = prefix;

            Random rnd = new Random(DateTime.Now.Millisecond);

            for (int i = 0; i < 10; i++)
            {
                publicName += (char)rnd.Next(65, 90);
                publicName += (char)rnd.Next(97, 122);
            }

            publicName = $"{prefix}{publicName}{suffix}";

            return publicName;
        }
    }
}
