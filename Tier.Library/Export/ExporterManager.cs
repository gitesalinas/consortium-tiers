﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Library.Export.Contract;
using Tier.Library.Export.Exporter;

namespace Tier.Library.Export
{
    public class ExporterManager
    {
        //public enum EnumExporter
        //{
        //    Excel,
        //    Csv,
        //    Pdf
        //}

        private IExporter Exporter { get; set; }

        //private ExporterManager(EnumExporter @enum)
        //{
        //    switch (@enum)
        //    {
        //        case EnumExporter.Excel: Exporter = new ExcelExporter(); break;
        //        case EnumExporter.Csv: Exporter = new CsvExporter(); break;
        //        case EnumExporter.Pdf: Exporter = new PdfExporter(); break;
        //        default: throw new Exception("Invalid Exporter");
        //    }
        //}

        private ExporterManager(IExporter exporter)
        {
            Exporter = exporter;
        }

        public void Export()
        {
            Exporter.Export();
        }

        public static ExporterManager CreateExcelExporterManager()
        {
            return new ExporterManager(new ExcelExporter());
        }

        public static ExporterManager CreateCsvExporterManager()
        {
            return new ExporterManager(new CsvExporter());
        }

        public static ExporterManager CreatePdfExporterManager()
        {
            return new ExporterManager(new PdfExporter());
        }
    }
}
