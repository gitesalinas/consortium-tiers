﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Library.Helper.File.Contract
{
    public interface IImageProcessor
    {
        void Resize(string imagePath, int width, int height, int quality);
        void Resize(string imagePath, string newImagePath, int width, int height, int quality);
        void Rotate(string imagePath, float degrees);
        void Rotate(string imagePath, string newImagePath, float degrees);
    }
}
