﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Tier.Library.Helper.File
{
    public class FileHelper
    {
        public enum EnumPath
        {
            Physical,
            Virtual
        }

        private static Random _random = new Random(DateTime.Now.Millisecond);

        public static string DEFAULT_IMAGES_PATH { get { return HttpContext.Current.Server.MapPath("~/Content/Uploads/Images"); } }
        public static string DEFAULT_SOUNDS_PATH { get { return HttpContext.Current.Server.MapPath("~/Content/Uploads/Audios"); } }
        public static string DEFAULT_VIDEOS_PATH { get { return HttpContext.Current.Server.MapPath("~/Content/Uploads/Videos"); } }
        public static string DEFAULT_FILES_PATH { get { return HttpContext.Current.Server.MapPath("~/Content/Uploads/Files"); } }

        public static string DEFAULT_IMAGES_VIRTUAL_PATH { get { return VirtualPathUtility.ToAbsolute("~/Content/Uploads/Images"); } }
        public static string DEFAULT_SOUNDS_VIRTUAL_PATH { get { return VirtualPathUtility.ToAbsolute("~/Content/Uploads/Audios"); } }
        public static string DEFAULT_VIDEOS_VIRTUAL_PATH { get { return VirtualPathUtility.ToAbsolute("~/Content/Uploads/Videos"); } }
        public static string DEFAULT_FILES_VIRTUAL_PATH { get { return VirtualPathUtility.ToAbsolute("~/Content/Uploads/Files"); } }

        public string FileName { get; protected set; }
        public string FilePublicName { get; set; }
        public string FileExtension { get; protected set; }

        public int FileContentLength { get; protected set; }
        public string FileContentType { get; protected set; }

        public string FileDirectoryPath { get; protected set; }
        public string FilePath { get; protected set; }
        public string FileVirtualDirectoryPath { get; protected set; }
        public string FileVirtualPath { get; protected set; }

        HttpPostedFileBase _file;
        public HttpPostedFileBase File
        {
            get { return _file; }
            set { _file = value; if (null != value) ResolveFile(); }
        }

        public FileHelper() : this(null, null)
        {
        }

        public FileHelper(HttpPostedFileBase file, string publicName)
        {
            FilePublicName = publicName;
            File = file;
        }

        public void Save()
        {
            if (!Directory.Exists(FileDirectoryPath))
                Directory.CreateDirectory(FileDirectoryPath);

            File.SaveAs(FilePath);
        }

        public virtual void CreatePaths(string uri = null)
        {
            if (String.IsNullOrEmpty(uri))
            {
                FileDirectoryPath = GetPathFromExtension(FileExtension, EnumPath.Physical);
                if (String.IsNullOrEmpty(FileDirectoryPath))
                    throw new Exception("Invalid Directory Path");
            }
            else
            {
                FileDirectoryPath = HttpContext.Current.Server.MapPath(uri);
            }

            FilePath = Path.Combine(FileDirectoryPath, (String.IsNullOrEmpty(FilePublicName) ? FileName : FilePublicName));

            if (String.IsNullOrEmpty(uri))
            {
                FileVirtualDirectoryPath = GetPathFromExtension(FileExtension, EnumPath.Virtual);
                if (String.IsNullOrEmpty(FileVirtualDirectoryPath))
                    throw new Exception("Invalid Virtual Directory Path");
            }
            else
            {
                FileVirtualDirectoryPath = VirtualPathUtility.ToAbsolute(uri);
            }

            FileVirtualPath = $"{FileVirtualDirectoryPath}/{(String.IsNullOrEmpty(FilePublicName) ? FileName : FilePublicName)}";
        }

        public virtual void AppendUriToPaths(string suffix)
        {
            if (!String.IsNullOrEmpty(suffix))
            {
                FileDirectoryPath = Path.Combine(FileDirectoryPath, suffix);
                FilePath = Path.Combine(FileDirectoryPath, (String.IsNullOrEmpty(FilePublicName) ? FileName : FilePublicName));

                FileVirtualDirectoryPath += $"/{suffix}";
                FileVirtualPath = $"{FileVirtualDirectoryPath}/{(String.IsNullOrEmpty(FilePublicName) ? FileName : FilePublicName)}";
            }
        }

        protected virtual void ResolveFile()
        {
            FileName = File.FileName;
            if (String.IsNullOrEmpty(FileName))
                throw new Exception("Invalid Filename");

            FileExtension = Path.GetExtension(FileName).ToLower();
            if (String.IsNullOrEmpty(FileExtension))
                throw new Exception("Invalid Extension");

            FileContentType = File.ContentType;
            if (String.IsNullOrEmpty(FileExtension))
                throw new Exception("Invalid Content Type");

            FileContentLength = File.ContentLength;
            if (0 >= FileContentLength)
                throw new Exception("Invalid Content Length");

            if(String.IsNullOrEmpty(FilePublicName)) CreatePublicName();

            //FileDirectoryPath = GetPathFromExtension(FileExtension, EnumPath.Physical);
            //if (String.IsNullOrEmpty(FileDirectoryPath))
            //    throw new Exception("Invalid Directory Path");

            //FilePath = Path.Combine(FileDirectoryPath, (String.IsNullOrEmpty(FilePublicName) ? FileName : FilePublicName));

            //FileVirtualDirectoryPath = GetPathFromExtension(FileExtension, EnumPath.Virtual);
            //if (String.IsNullOrEmpty(FileVirtualDirectoryPath))
            //    throw new Exception("Invalid Virtual Directory Path");

            //FileVirtualPath = $"{FileVirtualDirectoryPath}/{(String.IsNullOrEmpty(FilePublicName) ? FileName : FilePublicName)}";
        }

        public virtual void Clear()
        {
            FileName = null;
            FilePublicName = null;
            FileExtension = null;
            FileDirectoryPath = null;
            FilePath = null;
            FileVirtualDirectoryPath = null;
            FileVirtualPath = null;
            FileContentLength = 0;
            FileContentType = null;
        }

        public string CreatePublicName(string preffix = "", string suffix = "")
        {
            FilePublicName = $"{GeneratePublicName(preffix, suffix)}{FileExtension}";

            CreatePaths();

            return FilePublicName;
        }

        public static string GeneratePublicName(string preffix = "", string suffix = "")
        {
            string publicName = DateTime.Now.Ticks.ToString("x");

            //DateTime.Now.ToString().GetHashCode().ToString("x");
            //DateTime.Now.Ticks.ToString("x")

            for (int i = 0; i < 3; i++)
            {
                publicName += (_random.Next(0, 2) == 0 ?
                    (char)_random.Next(65, 91) : (char)_random.Next(97, 123));
            }

            return $"{preffix}{publicName}{suffix}";
        }
        public static string GetMediaTypeFromExtension(string extension)
        {
            string media = null;

            switch (extension)
            {
                case ".jpg":
                case ".jpeg":
                case ".png":
                case ".gif":
                case ".ico":
                case ".bmp": media = "image"; break;

                case ".avi":
                case ".flv":
                case ".3g2":
                case ".3gp":
                case ".m4v":
                case ".mov":
                case ".mpg":
                case ".swf":
                case ".vob":
                case ".wmv":
                case ".mp4": media = "video"; break;


                case ".pdf":
                case ".doc":
                case ".docx":
                case ".rtf":
                case ".txt":
                case ".csv":
                case ".pps":
                case ".ppt":
                case ".pptx":
                case ".xlr":
                case ".xls":
                case ".xlsx":
                case ".xml": media = "file"; break;


                //case ".rar":
                //case ".zip":
                //case ".7z":
                //case ".tar": media = "compress"; break;


                case ".m4a":
                case ".mid":
                case ".mpa":
                case ".wav":
                case ".wma":
                case ".mp3": media = "sound"; break;

                default: media = "file"; break;
            }

            return media;
        }
        public static string GetPathFromExtension(string extension, EnumPath @enum)
        {
            switch (extension)
            {
                case ".jpg":
                case ".jpeg":
                case ".png":
                case ".gif":
                case ".ico":
                case ".bmp": return !@enum.Equals(EnumPath.Virtual) ? DEFAULT_IMAGES_PATH : DEFAULT_IMAGES_VIRTUAL_PATH;

                case ".avi":
                case ".flv":
                case ".3g2":
                case ".3gp":
                case ".m4v":
                case ".mov":
                case ".mpg":
                case ".swf":
                case ".vob":
                case ".wmv":
                case ".mp4": return !@enum.Equals(EnumPath.Virtual) ? DEFAULT_VIDEOS_PATH : DEFAULT_VIDEOS_VIRTUAL_PATH;

                //case ".rar":
                //case ".zip":
                //case ".7z":
                //case ".tar": media = "compress"; break;

                case ".m4a":
                case ".mid":
                case ".mpa":
                case ".wav":
                case ".wma":
                case ".mp3": return !@enum.Equals(EnumPath.Virtual) ? DEFAULT_SOUNDS_PATH : DEFAULT_SOUNDS_VIRTUAL_PATH;

                case ".pdf":
                case ".doc":
                case ".docx":
                case ".rtf":
                case ".txt":
                case ".csv":
                case ".pps":
                case ".ppt":
                case ".pptx":
                case ".xlr":
                case ".xls":
                case ".xlsx":
                case ".xml":
                default: return !@enum.Equals(EnumPath.Virtual) ? DEFAULT_FILES_PATH : DEFAULT_FILES_VIRTUAL_PATH;
            }
        }
        public static string GetPathFromMediaType(string mediaType, EnumPath @enum)
        {
            switch (mediaType)
            {
                case "image": return !@enum.Equals(EnumPath.Virtual) ? DEFAULT_IMAGES_PATH : DEFAULT_IMAGES_VIRTUAL_PATH;
                case "video": return !@enum.Equals(EnumPath.Virtual) ? DEFAULT_VIDEOS_PATH : DEFAULT_VIDEOS_VIRTUAL_PATH;
                case "sound": return !@enum.Equals(EnumPath.Virtual) ? DEFAULT_SOUNDS_PATH : DEFAULT_SOUNDS_VIRTUAL_PATH;
                case "file":
                default: return !@enum.Equals(EnumPath.Virtual) ? DEFAULT_FILES_PATH : DEFAULT_FILES_VIRTUAL_PATH;
            }
        }
    }
}