﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Tier.Library.Helper.File.Contract;

namespace Tier.Library.Helper.File
{
    public class ImageFileHelper : FileHelper
    {
        public enum EnumThumbnailSize
        {
            XLarge,
            Large,
            Medium,
            Small,
            XSmall
        }
        public enum EnumImageQuality
        {
            High,
            Medium,
            Low
        }

        public const int DEFAULT_THUMBNAIL_WIDTH_XLARGE = 756;
        public const int DEFAULT_THUMBNAIL_HEIGHT_XLARGE = 756;
        public const int DEFAULT_THUMBNAIL_WIDTH_LARGE = 520;
        public const int DEFAULT_THUMBNAIL_HEIGHT_LARGE = 520;
        public const int DEFAULT_THUMBNAIL_WIDTH_MEDIUM = 256;
        public const int DEFAULT_THUMBNAIL_HEIGHT_MEDIUM = 256;
        public const int DEFAULT_THUMBNAIL_WIDTH_SMALL = 128;
        public const int DEFAULT_THUMBNAIL_HEIGHT_SMALL = 128;
        public const int DEFAULT_THUMBNAIL_WIDTH_XSMALL = 64;
        public const int DEFAULT_THUMBNAIL_HEIGHT_XSMALL = 64;

        private const int DEFAULT_THUMBNAIL_HIGH_QUALITY = 100;
        private const int DEFAULT_THUMBNAIL_MEDIUM_QUALITY = 50;
        private const int DEFAULT_THUMBNAIL_LOW_QUALITY = 30;

        public string FileThumbnailDirectoryPath { get; private set; }
        public string FileThumbnailPath { get; private set; }
        public string FileThumbnailVirtualDirectoryPath { get; private set; }
        public string FileThumbnailVirtualPath { get; private set; }

        public int FileThumbnailWidth { get; set; }
        public int FileThumbnailHeight { get; set; }
        public int FileThumbnailQuality { get; set; }

        public IImageProcessor Processor { get; set; }

        public ImageFileHelper() : 
            this(new ImageHelper(), null, null)
        {

        }

        public ImageFileHelper(string publicName) : 
            this(new ImageHelper(), null, publicName)
        {

        }

        public ImageFileHelper(HttpPostedFileBase file) : 
            this(file, null)
        {

        }

        public ImageFileHelper(HttpPostedFileBase file, string publicName) : 
            this(new ImageHelper(), file, publicName)
        {

        }

        public ImageFileHelper(IImageProcessor processor) : 
            this(processor, null, null)
        {

        }

        public ImageFileHelper(IImageProcessor processor, HttpPostedFileBase file, string publicName) : base(file, publicName)
        {
            Processor = processor;
        }


        public override void CreatePaths(string uri = null)
        {
            base.CreatePaths(uri);

            FileThumbnailDirectoryPath = Path.Combine(FileDirectoryPath, "Thumbnail");
            FileThumbnailPath = Path.Combine(FileThumbnailDirectoryPath, (String.IsNullOrEmpty(FilePublicName) ? FileName : FilePublicName));

            FileThumbnailVirtualDirectoryPath = $"{FileVirtualDirectoryPath}/Thumbnail";
            FileThumbnailVirtualPath = $"{FileThumbnailVirtualDirectoryPath}/{(String.IsNullOrEmpty(FilePublicName) ? FileName : FilePublicName)}";            
        }

        public override void AppendUriToPaths(string suffix)
        {
            base.AppendUriToPaths(suffix);

            if (!String.IsNullOrEmpty(suffix))
            {
                FileThumbnailDirectoryPath = Path.Combine(FileThumbnailDirectoryPath, suffix);
                FileThumbnailPath = Path.Combine(FileThumbnailDirectoryPath, (String.IsNullOrEmpty(FilePublicName) ? FileName : FilePublicName));

                FileThumbnailVirtualDirectoryPath += $"/{suffix}";
                FileThumbnailVirtualPath = $"{FileThumbnailVirtualDirectoryPath}/{(String.IsNullOrEmpty(FilePublicName) ? FileName : FilePublicName)}";
            }
        }


        public void SetThumbnailSize(EnumThumbnailSize @enum)
        {
            switch (@enum)
            {
                case EnumThumbnailSize.Large:
                    FileThumbnailWidth = DEFAULT_THUMBNAIL_WIDTH_LARGE;
                    FileThumbnailHeight = DEFAULT_THUMBNAIL_HEIGHT_LARGE;
                    break;
                case EnumThumbnailSize.Medium:
                    FileThumbnailWidth = DEFAULT_THUMBNAIL_WIDTH_MEDIUM;
                    FileThumbnailHeight = DEFAULT_THUMBNAIL_HEIGHT_MEDIUM;
                    break;
                case EnumThumbnailSize.Small:
                default:
                    FileThumbnailWidth = DEFAULT_THUMBNAIL_WIDTH_SMALL;
                    FileThumbnailHeight = DEFAULT_THUMBNAIL_HEIGHT_SMALL;
                    break;
            }
        }
        public void SetThumbnailQuality(EnumImageQuality @enum)
        {
            switch (@enum)
            {
                case EnumImageQuality.High:
                    FileThumbnailQuality = DEFAULT_THUMBNAIL_HIGH_QUALITY;
                    break;
                case EnumImageQuality.Medium:
                    FileThumbnailQuality = DEFAULT_THUMBNAIL_MEDIUM_QUALITY;
                    break;
                case EnumImageQuality.Low:
                default:
                    FileThumbnailQuality = DEFAULT_THUMBNAIL_LOW_QUALITY;
                    break;
            }
        }


        protected override void ResolveFile()
        {
            base.ResolveFile();

            //FileThumbnailDirectoryPath = Path.Combine(FileDirectoryPath, "Thumbnail");
            //FileThumbnailPath = Path.Combine(FileThumbnailDirectoryPath, (String.IsNullOrEmpty(FilePublicName) ? FileName : FilePublicName));

            //FileThumbnailVirtualDirectoryPath = $"{FileVirtualDirectoryPath}/Thumbnail";
            //FileThumbnailVirtualPath = $"{FileThumbnailVirtualDirectoryPath}/{(String.IsNullOrEmpty(FilePublicName) ? FileName : FilePublicName)}";

            if(0 == FileThumbnailWidth)
            {
                FileThumbnailWidth = DEFAULT_THUMBNAIL_WIDTH_MEDIUM;
            }

            if (0 == FileThumbnailHeight)
            {
                FileThumbnailHeight = DEFAULT_THUMBNAIL_HEIGHT_MEDIUM;
            }

            if (0 == FileThumbnailQuality)
            {
                FileThumbnailQuality = DEFAULT_THUMBNAIL_HIGH_QUALITY;
            }
        }

        public override void Clear()
        {
            base.Clear();

            FileThumbnailDirectoryPath = null;
            FileThumbnailPath = null;
            FileThumbnailVirtualDirectoryPath = null;
            FileThumbnailVirtualPath = null;
            FileThumbnailWidth = 0;
            FileThumbnailHeight = 0;
            FileThumbnailQuality = DEFAULT_THUMBNAIL_HIGH_QUALITY;
        }

        public void SaveWithThumbnail()
        {
            base.Save();
            this.SaveThumbnail();
        }

        public void SaveThumbnail()
        {
            if (!System.IO.File.Exists(FilePath)) throw new Exception("File doesn't exists");

            if (!Directory.Exists(FileThumbnailDirectoryPath))
                Directory.CreateDirectory(FileThumbnailDirectoryPath);

            Processor.Resize(FilePath, FileThumbnailPath, FileThumbnailWidth, FileThumbnailHeight, FileThumbnailQuality);
        }
    }
}
