﻿using ImageProcessor;
using ImageProcessor.Imaging;
using ImageProcessor.Imaging.Formats;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Library.Helper.File.Contract;

namespace Tier.Library.Helper
{
    public class ImageHelper : IImageProcessor
    {
        public ImageHelper()
        {

        }

        public void Resize(string imagePath, int width, int height, int quality)
        {
            Resize(imagePath, imagePath, width, height, quality);
        }

        public void Resize(string imagePath, string newImagePath, int width, int height, int quality)
        {
            byte[] photoBytes = System.IO.File.ReadAllBytes(imagePath);
            // Format is automatically detected though can be changed.
            ISupportedImageFormat format = new JpegFormat { Quality = quality };
            Size size = new Size(width, height);
            using (MemoryStream inStream = new MemoryStream(photoBytes))
            {
                using (MemoryStream outStream = new MemoryStream())
                {
                    // Initialize the ImageFactory using the overload to preserve EXIF metadata.
                    using (ImageFactory imageFactory = new ImageFactory(preserveExifData: true))
                    {
                        ResizeLayer resizeLayer = new ResizeLayer(size, ResizeMode.Max, AnchorPosition.Center);
                        // Load, resize, set the format and quality and save an image.
                        imageFactory.Load(inStream)
                                    .Resize(resizeLayer)
                                    //.BackgroundColor(Color.FromArgb(0, 255, 255, 255))
                                    .Format(format)
                                    .Save(outStream);
                    }
                    // Do something with the stream.
                    FileStream file = new FileStream(newImagePath, FileMode.Create, FileAccess.Write);
                    outStream.WriteTo(file);
                    file.Close();
                }
            }
        }


        public void Rotate(string imagePath, float degrees)
        {
            Rotate(imagePath, imagePath, degrees);
        }

        public void Rotate(string imagePath, string newImagePath, float degrees)
        {
            byte[] photoBytes = System.IO.File.ReadAllBytes(imagePath);

            using (MemoryStream inStream = new MemoryStream(photoBytes))
            {
                using (MemoryStream outStream = new MemoryStream())
                {
                    // Initialize the ImageFactory using the overload to preserve EXIF metadata.
                    using (ImageFactory imageFactory = new ImageFactory(preserveExifData: true))
                    {
                        // Load, resize, set the format and quality and save an image.
                        imageFactory.Load(inStream)
                                    .Rotate(degrees)
                                    .Save(outStream);
                    }
                    // Do something with the stream.
                    FileStream file = new FileStream(newImagePath, FileMode.Create, FileAccess.Write);
                    outStream.WriteTo(file);
                    file.Close();
                }
            }
        }
    }
}
