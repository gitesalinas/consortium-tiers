﻿using OrmRepository = Tier.Data.DataAccess.Db.Orm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Factory.AbstractFactory;
using Tier.Data.Factory.ConcreteFactory;
using Tier.Data.Contract;
using Tier.Entity;

namespace Tier.Service
{
    public class UserTypeService
    {
        #region Singleton
        private static UserTypeService _instance;
        public static UserTypeService Instance { get { return _instance ?? (_instance = new UserTypeService()); } }
        private UserTypeService()
        {

        }
        #endregion

        #region Factory

        private readonly AbstractGenericFactory Factory = new ConcreteGenericFactory();

        private IUserTypeRepository UserTypeRepository
        {
            get
            {
                return Factory.CreateObject<OrmRepository.UserTypeRepository>();
            }
        }

        #endregion

        public IEnumerable<UserType> GetAll()
        {
            return UserTypeRepository.GetAll();
        }

        public UserType Get(int userTypeId)
        {
            return UserTypeRepository.Get(userTypeId);
        }

        public void Save(UserType userType)
        {
            UserTypeRepository.Save(userType);
        }

        public void Delete(UserType userType)
        {
            UserTypeRepository.Delete(userType);
        }
    }
}
