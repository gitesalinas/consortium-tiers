﻿using OrmRepository = Tier.Data.DataAccess.Db.Orm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Data.Factory.AbstractFactory;
using Tier.Data.Factory.ConcreteFactory;
using Tier.Entity;

namespace Tier.Service
{
    public class RoomMediaService
    {
        #region Singleton
        private static RoomMediaService _instance;
        public static RoomMediaService Instance { get { return _instance ?? (_instance = new RoomMediaService()); } }
        private RoomMediaService()
        {

        }
        #endregion

        #region Factory

        private readonly AbstractGenericFactory Factory = new ConcreteGenericFactory();

        private IRoomMediaRepository RoomMediaRepository
        {
            get
            {
                return Factory.CreateObject<OrmRepository.RoomMediaRepository>();
            }
        }

        #endregion

        public IEnumerable<RoomMedia> GetAll()
        {
            return RoomMediaRepository.GetAll();
        }

        public RoomMedia Get(int id)
        {
            return RoomMediaRepository.Get(id);
        }

        public IEnumerable<RoomMedia> GetAllByRoomId(int roomId)
        {
            return RoomMediaRepository.GetAllByRoomId(roomId);
        }

        public void Save(RoomMedia roomMedia)
        {
            RoomMediaRepository.Save(roomMedia);
        }

        public void Delete(RoomMedia roomMedia)
        {
            RoomMediaRepository.Delete(roomMedia);
        }

        public void HandleMain(RoomMedia roomMedia)
        {
            RoomMediaRepository.HandleMain(roomMedia);
        }

        public RoomMedia GetMainRoomMedia(int roomId)
        {
            return RoomMediaRepository.GetMainRoomMedia(roomId);
        }

        public void AddRoomMedia(RoomMedia roomMedia)
        {
            RoomMediaRepository.AddRoomMedia(roomMedia);
        }
    }
}
