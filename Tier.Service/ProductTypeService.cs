﻿using OrmRepository = Tier.Data.DataAccess.Db.Orm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Factory.AbstractFactory;
using Tier.Data.Factory.ConcreteFactory;
using Tier.Data.Contract;
using Tier.Entity;

namespace Tier.Service
{
    public class ProductTypeService
    {
        #region Singleton
        private static ProductTypeService _instance;
        public static ProductTypeService Instance { get { return _instance ?? (_instance = new ProductTypeService()); } }
        private ProductTypeService()
        {

        }
        #endregion

        #region Factory

        private readonly AbstractGenericFactory Factory = new ConcreteGenericFactory();

        private IProductTypeRepository ProductTypeRepository
        {
            get
            {
                return Factory.CreateObject<OrmRepository.ProductTypeRepository>();
            }
        }

        #endregion

        public IEnumerable<ProductType> GetAll()
        {
            return ProductTypeRepository.GetAll();
        }

        public ProductType Get(int productTypeId)
        {
            return ProductTypeRepository.Get(productTypeId);
        }

        public void Save(ProductType productType)
        {
            ProductTypeRepository.Save(productType);
        }

        public void Delete(ProductType productType)
        {
            ProductTypeRepository.Delete(productType);
        }
    }
}
