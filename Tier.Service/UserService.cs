﻿using OrmRepository = Tier.Data.DataAccess.Db.Orm.Repository;
using HttpRepository = Tier.Data.DataAccess.Http.Repository;
using Tier.Data.Contract;
using Tier.Data.Factory.AbstractFactory;
using Tier.Data.Factory.ConcreteFactory;
using Tier.Entity;
using static Tier.Data.Factory.AbstractFactory.AbstractGenericFactory;
using System.Collections.Generic;
using System;

namespace Tier.Service
{
    public class UserService
    {
        #region Singleton
        private static UserService _instance;
        public static UserService Instance { get { return _instance ?? (_instance = new UserService()); } }
        private UserService()
        {

        }
        #endregion

        #region Factory

        private readonly AbstractGenericFactory Factory = new ConcreteGenericFactory();

        private IUserRepository UserRepositoryOrm
        {
            get
            {
                return Factory.CreateObject<OrmRepository.UserRepository>();
            }
        }

        private IUserRepository UserRepositoryHttp
        {
            get
            {
                return Factory.CreateObject<HttpRepository.UserRepository>();
            }
        }

        #endregion

        public User Login(string username, string password)
        {
            return UserRepositoryOrm.Login(username, password);
        }

        public User LoginAPI(string username, string password)
        {
            return UserRepositoryHttp.Login(username, password);
        }


        public IEnumerable<User> GetAllFull()
        {
            return UserRepositoryOrm.GetAllFull();
        }

        public IEnumerable<User> GetAll()
        {
            return UserRepositoryOrm.GetAll();
        }

        public User Get(int id)
        {
            return UserRepositoryOrm.Get(id);
        }

        public User GetWithPerson(int id)
        {
            return UserRepositoryOrm.GetWithPerson(id);
        }


        public User GetProfile(int id)
        {
            return UserRepositoryOrm.GetProfile(id);
        }

        public void SaveProfile(User user)
        {
            UserRepositoryOrm.SaveProfile(user);
        }

        public void SaveProfilePicture(User user, Resource resource)
        {
            UserRepositoryOrm.SaveProfilePicture(user, resource);
        }

        public void Save(User user)
        {
            UserRepositoryOrm.Save(user);
        }

        public void Delete(int id)
        {
            UserRepositoryOrm.Delete(id);
        }
    }
}