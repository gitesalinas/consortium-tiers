﻿using OrmRepository = Tier.Data.DataAccess.Db.Orm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Data.Factory.AbstractFactory;
using Tier.Data.Factory.ConcreteFactory;
using Tier.Entity;

namespace Tier.Service
{
    public class ProductLoggingService
    {
        #region Singleton
        private static ProductLoggingService _instance;
        public static ProductLoggingService Instance { get { return _instance ?? (_instance = new ProductLoggingService()); } }
        private ProductLoggingService()
        {

        }
        #endregion

        #region Factory

        private readonly AbstractGenericFactory Factory = new ConcreteGenericFactory();

        private IProductLoggingRepository ProductRepository
        {
            get
            {
                return Factory.CreateObject<OrmRepository.ProductLoggingRepository>();
            }
        }

        #endregion

        public IEnumerable<ProductLogging> GetAll()
        {
            return ProductRepository.GetAll();
        }

        public ProductLogging Get(int productId)
        {
            return ProductRepository.Get(productId);
        }

        public void Delete(ProductLogging product)
        {
            ProductRepository.Delete(product);
        }

        public void Save(ProductLogging product)
        {
            ProductRepository.Save(product);
        }
    }
}
