﻿using OrmRepository = Tier.Data.DataAccess.Db.Orm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Data.Factory.AbstractFactory;
using Tier.Data.Factory.ConcreteFactory;
using Tier.Entity;

namespace Tier.Service
{
    public class StatusService
    {
        #region Singleton
        private static StatusService _instance;
        public static StatusService Instance { get { return _instance ?? (_instance = new StatusService()); } }
        private StatusService()
        {

        }
        #endregion

        #region Factory

        private readonly AbstractGenericFactory Factory = new ConcreteGenericFactory();

        private IStatusRepository StatusRepository
        {
            get
            {
                return Factory.CreateObject<OrmRepository.StatusRepository>();
            }
        }

        #endregion

        public IEnumerable<StatusEntity> GetAll()
        {
            return StatusRepository.GetAll();
        }

        public StatusEntity Get(int userTypeId)
        {
            return StatusRepository.Get(userTypeId);
        }
    }
}
