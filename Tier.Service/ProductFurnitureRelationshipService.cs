﻿using OrmRepository = Tier.Data.DataAccess.Db.Orm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Data.Factory.AbstractFactory;
using Tier.Data.Factory.ConcreteFactory;
using Tier.Entity;

namespace Tier.Service
{
    public class ProductFurnitureRelationshipService
    {
        #region Singleton
        private static ProductFurnitureRelationshipService _instance;
        public static ProductFurnitureRelationshipService Instance { get { return _instance ?? (_instance = new ProductFurnitureRelationshipService()); } }
        private ProductFurnitureRelationshipService()
        {

        }
        #endregion

        #region Factory

        private readonly AbstractGenericFactory Factory = new ConcreteGenericFactory();

        private IProductFurnitureRelationshipRepository ProductRelationshipRepository
        {
            get
            {
                return Factory.CreateObject<OrmRepository.ProductFurnitureRelationshipRepository>();
            }
        }

        #endregion

        public IEnumerable<ProductFurnitureRelationship> GetAll()
        {
            return ProductRelationshipRepository.GetAll();
        }

        public IEnumerable<ProductFurnitureRelationship> GetAllByProductOwner(int productOwnerId)
        {
            return ProductRelationshipRepository.GetAllByProductOwner(productOwnerId);
        }


        public ProductFurnitureRelationship Get(int productRelationshipId)
        {
            return ProductRelationshipRepository.Get(productRelationshipId);
        }

        public void Save(ProductFurnitureRelationship productRelationship)
        {
            ProductRelationshipRepository.Save(productRelationship);
        }

        public void Delete(ProductFurnitureRelationship productRelationship)
        {
            ProductRelationshipRepository.Delete(productRelationship);
        }

        public ProductFurnitureRelationship GetByProductOwnerAndProductDependant(int productOwnerId, int productDependantId)
        {
            return ProductRelationshipRepository.GetByProductOwnerAndProductDependant(productOwnerId, productDependantId);
        }
    }
}
