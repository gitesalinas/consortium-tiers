﻿using OrmRepository = Tier.Data.DataAccess.Db.Orm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Data.Factory.AbstractFactory;
using Tier.Data.Factory.ConcreteFactory;
using Tier.Entity;

namespace Tier.Service
{
    public class FacilityService
    {
        #region Singleton
        private static FacilityService _instance;
        public static FacilityService Instance { get { return _instance ?? (_instance = new FacilityService()); } }
        private FacilityService()
        {

        }
        #endregion

        #region Factory

        private readonly AbstractGenericFactory Factory = new ConcreteGenericFactory();

        private IFacilityRepository FacilityRepository
        {
            get
            {
                return Factory.CreateObject<OrmRepository.FacilityRepository>();
            }
        }

        #endregion

        public IEnumerable<Facility> GetAll()
        {
            return FacilityRepository.GetAll();
        }

        public Facility Get(int facilityId)
        {
            return FacilityRepository.Get(facilityId);
        }

        public void Save(Facility facility)
        {
            FacilityRepository.Save(facility);
        }

        public void Delete(Facility facility)
        {
            FacilityRepository.Delete(facility);
        }
    }
}
