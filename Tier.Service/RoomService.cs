﻿using OrmRepository = Tier.Data.DataAccess.Db.Orm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Factory.AbstractFactory;
using Tier.Data.Factory.ConcreteFactory;
using Tier.Data.Contract;
using Tier.Entity;

namespace Tier.Service
{
    public class RoomService
    {
        #region Singleton
        private static RoomService _instance;
        public static RoomService Instance { get { return _instance ?? (_instance = new RoomService()); } }
        private RoomService()
        {

        }
        #endregion

        #region Factory

        private readonly AbstractGenericFactory Factory = new ConcreteGenericFactory();

        private IRoomRepository RoomRepository
        {
            get
            {
                return Factory.CreateObject<OrmRepository.RoomRepository>();
            }
        }

        #endregion


        public IEnumerable<Room> GetAllFull()
        {
            return RoomRepository.GetAllFull();
        }

        public IEnumerable<Room> GetAll()
        {
            return RoomRepository.GetAll();
        }

        public Room Get(int id)
        {
            return RoomRepository.Get(id);
        }


        public void Save(Room room)
        {
            RoomRepository.Save(room);
        }

        public void Delete(Room room)
        {
            RoomRepository.Delete(room);
        }

        //public void AddRoomMedia(RoomMedia roomMedia)
        //{
        //    RoomRepository.AddRoomMedia(roomMedia);
        //}
    }
}
