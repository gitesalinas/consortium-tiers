﻿using OrmRepository = Tier.Data.DataAccess.Db.Orm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Data.Factory.AbstractFactory;
using Tier.Data.Factory.ConcreteFactory;
using Tier.Entity;

namespace Tier.Service
{
    public class ProductFurnitureService
    {
        #region Singleton
        private static ProductFurnitureService _instance;
        public static ProductFurnitureService Instance { get { return _instance ?? (_instance = new ProductFurnitureService()); } }
        private ProductFurnitureService()
        {

        }
        #endregion

        #region Factory

        private readonly AbstractGenericFactory Factory = new ConcreteGenericFactory();

        private IProductFurnitureRepository ProductRepository
        {
            get
            {
                return Factory.CreateObject<OrmRepository.ProductFurnitureRepository>();
            }
        }

        #endregion

        public IEnumerable<ProductFurniture> GetAll()
        {
            return ProductRepository.GetAll();
        }

        public ProductFurniture Get(int productId)
        {
            return ProductRepository.Get(productId);
        }

        public void Delete(ProductFurniture product)
        {
            ProductRepository.Delete(product);
        }

        public void Save(ProductFurniture product)
        {
            ProductRepository.Save(product);
        }

        public IEnumerable<ProductFurniture> GetAllForRelationship(int productId)
        {
            return ProductRepository.GetAllForRelationship(productId);
        }
    }
}
