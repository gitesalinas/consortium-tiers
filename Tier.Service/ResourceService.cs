﻿using OrmRepository = Tier.Data.DataAccess.Db.Orm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Data.Factory.AbstractFactory;
using Tier.Data.Factory.ConcreteFactory;
using Tier.Entity;
using Tier.Library.Common;

namespace Tier.Service
{
    public class ResourceService
    {
        #region Singleton
        private static ResourceService _instance;
        public static ResourceService Instance { get { return _instance ?? (_instance = new ResourceService()); } }
        private ResourceService()
        {

        }
        #endregion

        #region Factory

        private readonly AbstractGenericFactory Factory = new ConcreteGenericFactory();

        private IResourceRepository ResourceRepository
        {
            get
            {
                return Factory.CreateObject<OrmRepository.ResourceRepository>();
            }
        }

        #endregion

        public Resource Get(int resourceId)
        {
            return ResourceRepository.Get(resourceId);
        }

        public void Save(Resource resource)
        {
            ResourceRepository.Save(resource);
        }
    }
}
