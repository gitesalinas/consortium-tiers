﻿using OrmRepository = Tier.Data.DataAccess.Db.Orm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Data.Factory.AbstractFactory;
using Tier.Data.Factory.ConcreteFactory;
using Tier.Entity;

namespace Tier.Service
{
    public class ProductLoggingMediaService
    {
        #region Singleton
        private static ProductLoggingMediaService _instance;
        public static ProductLoggingMediaService Instance { get { return _instance ?? (_instance = new ProductLoggingMediaService()); } }
        private ProductLoggingMediaService()
        {

        }
        #endregion

        #region Factory

        private readonly AbstractGenericFactory Factory = new ConcreteGenericFactory();

        private IProductLoggingMediaRepository ProductMediaRepository
        {
            get
            {
                return Factory.CreateObject<OrmRepository.ProductLoggingMediaRepository>();
            }
        }

        #endregion

        public IEnumerable<ProductLoggingMedia> GetAll()
        {
            return ProductMediaRepository.GetAll();
        }

        public ProductLoggingMedia Get(int id)
        {
            return ProductMediaRepository.Get(id);
        }

        public IEnumerable<ProductLoggingMedia> GetAllByProductId(int productId)
        {
            return ProductMediaRepository.GetAllByProductId(productId);
        }

        public void Save(ProductLoggingMedia productMedia)
        {
            ProductMediaRepository.Save(productMedia);
        }

        public void Delete(ProductLoggingMedia productMedia)
        {
            ProductMediaRepository.Delete(productMedia);
        }

        public void HandleMain(ProductLoggingMedia productMedia)
        {
            ProductMediaRepository.HandleMain(productMedia);
        }

        public ProductLoggingMedia GetMainProductMedia(int productId)
        {
            return ProductMediaRepository.GetMainProductMedia(productId);
        }

        public void AddRoomMedia(ProductLoggingMedia productMedia)
        {
            ProductMediaRepository.AddProductMedia(productMedia);
        }
    }
}
