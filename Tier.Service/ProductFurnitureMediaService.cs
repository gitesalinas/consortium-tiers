﻿using OrmRepository = Tier.Data.DataAccess.Db.Orm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Factory.AbstractFactory;
using Tier.Data.Factory.ConcreteFactory;
using Tier.Data.Contract;
using Tier.Entity;

namespace Tier.Service
{
    public class ProductFurnitureMediaService
    {
        #region Singleton
        private static ProductFurnitureMediaService _instance;
        public static ProductFurnitureMediaService Instance { get { return _instance ?? (_instance = new ProductFurnitureMediaService()); } }
        private ProductFurnitureMediaService()
        {

        }
        #endregion

        #region Factory

        private readonly AbstractGenericFactory Factory = new ConcreteGenericFactory();

        private IProductFurnitureMediaRepository ProductMediaRepository
        {
            get
            {
                return Factory.CreateObject<OrmRepository.ProductFurnitureMediaRepository>();
            }
        }

        #endregion

        public IEnumerable<ProductFurnitureMedia> GetAll()
        {
            return ProductMediaRepository.GetAll();
        }

        public ProductFurnitureMedia Get(int id)
        {
            return ProductMediaRepository.Get(id);
        }

        public IEnumerable<ProductFurnitureMedia> GetAllByProductId(int productId)
        {
            return ProductMediaRepository.GetAllByProductId(productId);
        }

        public void Save(ProductFurnitureMedia product)
        {
            ProductMediaRepository.Save(product);
        }

        public void Delete(ProductFurnitureMedia product)
        {
            ProductMediaRepository.Delete(product);
        }

        public void HandleMain(ProductFurnitureMedia product)
        {
            ProductMediaRepository.HandleMain(product);
        }

        public ProductFurnitureMedia GetMainProductMedia(int productId)
        {
            return ProductMediaRepository.GetMainProductMedia(productId);
        }

        public void AddRoomMedia(ProductFurnitureMedia productMedia)
        {
            ProductMediaRepository.AddProductMedia(productMedia);
        }
    }
}
