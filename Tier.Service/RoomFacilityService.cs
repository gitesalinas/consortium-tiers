﻿using OrmRepository = Tier.Data.DataAccess.Db.Orm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Data.Contract;
using Tier.Data.Factory.AbstractFactory;
using Tier.Data.Factory.ConcreteFactory;
using Tier.Entity;

namespace Tier.Service
{
    public class RoomFacilityService
    {
        #region Singleton
        private static RoomFacilityService _instance;
        public static RoomFacilityService Instance { get { return _instance ?? (_instance = new RoomFacilityService()); } }
        private RoomFacilityService()
        {

        }
        #endregion

        #region Factory

        private readonly AbstractGenericFactory Factory = new ConcreteGenericFactory();

        private IRoomFacilityRepository RoomFacilityRepository
        {
            get
            {
                return Factory.CreateObject<OrmRepository.RoomFacilityRepository>();
            }
        }

        #endregion


        public RoomFacility Get(int id)
        {
            return RoomFacilityRepository.Get(id);
        }

        public RoomFacility GetAny(int id)
        {
            return RoomFacilityRepository.GetAny(id);
        }
        
        public RoomFacility GetAnyByRoomIdAndFacilityId(int roomId, int facilityId)
        {
            return RoomFacilityRepository.GetAnyByRoomIdAndFacilityId(roomId, facilityId);
        }

        public IEnumerable<RoomFacility> GetAllByRoomId(int roomId)
        {
            return RoomFacilityRepository.GetAllByRoomId(roomId);
        }

        public void Save(RoomFacility roomFacility)
        {
            RoomFacilityRepository.Save(roomFacility);
        }

        public void Delete(RoomFacility roomFacility)
        {
            RoomFacilityRepository.Delete(roomFacility);
        }
    }
}
