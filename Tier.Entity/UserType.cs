﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Entity
{
    [Serializable]
    public class UserType : BaseEntity
    {
        public UserType()
        {

        }

        public int Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
