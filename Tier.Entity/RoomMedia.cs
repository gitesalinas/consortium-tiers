﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Entity
{
    public class RoomMedia : BaseEntity
    {
        public bool IsMain { get; set; }

        public int RoomId { get; set; }
        public Room Room { get; set; }

        public int ResourceId { get; set; }
        public Resource Resource { get; set; }
    }
}
