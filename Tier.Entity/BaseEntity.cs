﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tier.Library.Common;

namespace Tier.Entity
{
    [Serializable]
    public abstract class BaseEntity
    {
        public int Id { get; set; }
        public short Status { get; set; }

        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<DateTime> UpdatedDate { get; set; }

        public void ResolveSecurityFields()
        {
            ResolveSecurityFields(0, DateTime.Now);
        }

        public void ResolveSecurityFields(int userId)
        {
            ResolveSecurityFields(userId, DateTime.Now);
        }

        public void ResolveSecurityFields(int userId, DateTime dateTime)
        {
            if (0 == Id) { CreatedBy = userId; CreatedDate = dateTime; }
            else { UpdatedBy = userId; UpdatedDate = dateTime; }
        }

        public void SetDisabled() { Status = (short)EnumStatus.Disabled; }

        public void SetEnabled() { Status = (short)EnumStatus.Enabled; }

        public void SetDeleted() { Status = (short)EnumStatus.Deleted; }
    }
}
