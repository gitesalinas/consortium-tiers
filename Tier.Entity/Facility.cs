﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Entity
{
    public class Facility : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
