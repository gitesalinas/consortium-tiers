﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Entity
{
    [Serializable]
    public abstract class BaseProduct : BaseEntity
    {
        public string SKU { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FullDescription { get; set; }

        public decimal Stock { get; set; }

        public decimal Tax { get; set; }
        public decimal UnitPrice { get; set; }

        public decimal TotalPrice { get; set; }

        public int? TypeId { get; set; }
        public ProductType Type { get; set; }
    }
}
