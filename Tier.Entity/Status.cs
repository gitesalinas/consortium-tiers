﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Entity
{
    public class StatusEntity : BaseEntity
    {
        public StatusEntity()
        {

        }

        public int Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
