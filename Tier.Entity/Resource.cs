﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Entity
{
    [Serializable]
    public class Resource : BaseEntity
    {
        public string Name { get; set; }
        public string Extension { get; set; }
        public int ContentLength { get; set; }
        public string ContentType { get; set; }
        public string PublicName { get; set; }
        public string DirectoryPath { get; set; }
        public string Path { get; set; }
        public string VirtualDirectoryPath { get; set; }
        public string VirtualPath { get; set; }

        public string ThumbnailDirectoryPath { get; set; }
        public string ThumbnailPath { get; set; }
        public string ThumbnailVirtualDirectoryPath { get; set; }
        public string ThumbnailVirtualPath { get; set; }

        public Resource()
        {

        }
    }
}
