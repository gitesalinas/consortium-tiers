﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Entity
{
    [Serializable]
    public class ProductLogging : BaseProduct
    {
        public List<ProductLoggingMedia> ProductMedias { get; set; }
        public List<ProductLoggingRelationship> ProductRelationship { get; set; }
    }
}
