﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Entity
{
    public class ProductFurnitureRelationship : BaseEntity
    {
        public int ProductOwnerId { get; set; }
        public ProductFurniture ProductOwner { get; set; }

        public int ProductDependantId { get; set; }
        public ProductFurniture ProductDependant { get; set; }

        public string SKU { get; set; }
        public string Name { get; set; }

        public decimal Units { get; set; }

        public decimal Tax { get; set; }
        public decimal UnitPrice { get; set; }

        public decimal TotalPrice { get; set; }
    }
}
