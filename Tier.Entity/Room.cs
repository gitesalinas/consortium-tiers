﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Entity
{
    public class Room : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string FullDescription { get; set; }

        public Decimal Price { get; set; }

        public List<RoomFacility> RoomFacilities { get; set; }
        public List<RoomMedia> RoomMedias { get; set; }
    }
}
