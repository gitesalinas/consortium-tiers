﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Entity
{
    [Serializable]
    public class ProductFurniture : BaseProduct
    {
        public List<ProductFurnitureMedia> ProductMedias { get; set; }
        public List<ProductFurnitureRelationship> ProductRelationship { get; set; }
    }
}
