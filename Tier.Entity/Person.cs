﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Entity
{
    [Serializable]
    public class Person : BaseEntity
    {
        public Person()
        {
            //Users = new List<User>();
        }

        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }

        public string Gender { get; set; }

        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MobilPhone { get; set; }

        public string Website { get; set; }

        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string GooglePlus { get; set; }
        public string Linkedin { get; set; }
        public string Skype { get; set; }

        public Nullable<DateTime> Birth { get; set; }

        public List<User> Users { get; set; }
    }
}
