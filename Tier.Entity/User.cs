﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Entity
{
    [Serializable]
    public class User : BaseEntity
    {
        public User()
        {
        }

        public string Username { get; set; }
        public string Password { get; set; }

        public int TypeId { get; set; }

        public UserType Type { get; set; }

        public int? PictureId { get; set; }
        public Resource Picture { get; set; }

        public int PersonId { get; set; }

        public Person Person { get; set; }
    }
}
