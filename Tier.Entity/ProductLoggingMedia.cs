﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tier.Entity
{
    [Serializable]
    public class ProductLoggingMedia : BaseEntity
    {
        public bool IsMain { get; set; }

        public int ProductId { get; set; }
        public ProductLogging Product { get; set; }

        public int ResourceId { get; set; }
        public Resource Resource { get; set; }
    }
}
